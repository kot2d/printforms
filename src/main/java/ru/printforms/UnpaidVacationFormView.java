package ru.printforms;

import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.spring.annotation.UIScope;
import org.springframework.beans.factory.annotation.Autowired;
import ru.printforms.domain.SecuredUser;
import ru.printforms.domain.applicationForms.UnpaidVacationApplicationForm;
import ru.printforms.services.ApplicationFormEvent;
import ru.printforms.services.ApplicationFormLayout;
import ru.printforms.services.vaadin.DateTimePeriodLayout;
import ru.printforms.services.vaadin.FormProperties;
import ru.printforms.services.vaadin.binders.UnpaidVacationFormBinder;

@Route("unpaid_vacation_form")
@UIScope
public class UnpaidVacationFormView extends VerticalLayout {

    UnpaidVacationApplicationForm document;
    private DateTimePeriodLayout datePeriodLayout = new DateTimePeriodLayout.Builder(FormProperties.INPUT_WIDTH_INT)
            .withDatePeriod()
            .withCheckPeriodOrOneTime("На один день")
            .withDescription("Укажите даты начала и окончания нового графика:")
            .build();

    private final DatePicker startDatePicker = datePeriodLayout.getFirstDay();
    private final DatePicker endDatePicker = datePeriodLayout.getEndDate();
    private final Checkbox oneDayVacation = datePeriodLayout.getPeriodOrOneTime();

    private ApplicationFormLayout<UnpaidVacationApplicationForm> appFormLayout;
    ApplicationFormEvent applicationFormEvent = new ApplicationFormEvent();

    public UnpaidVacationFormView(@Autowired SecuredUser user) {
        this.document = new UnpaidVacationApplicationForm();

        UnpaidVacationFormBinder formBinder =
                new UnpaidVacationFormBinder(document, new Binder<>(UnpaidVacationApplicationForm.class));

        formBinder.bindStartDate(this.startDatePicker);
        formBinder.bindEndDate(this.endDatePicker, this.startDatePicker);

        this.appFormLayout = new ApplicationFormLayout<>(user, formBinder, this.document, this.applicationFormEvent);

        VerticalLayout inputLayout = this.appFormLayout.getCustomInputLayout();
        startDatePicker.addValueChangeListener(valueChangeEvent -> {
            this.buildBody();
        });

        this.oneDayVacation.addValueChangeListener(valueChangeEvent ->{
            if(this.oneDayVacation.getValue()){
                this.endDatePicker.setVisible(false);
                this.buildBody();
            } else {
                this.endDatePicker.setVisible(true);
                this.buildBody();
            }
        });
        endDatePicker.addValueChangeListener(valueChangeEvent -> {
            this.buildBody();
        });
        inputLayout.add(datePeriodLayout);
        this.add(appFormLayout);
        this.buildBody();
    }

    private void buildBody(){
        if(this.oneDayVacation.getValue())
            this.document.setBody(this.startDatePicker.getValue());
        else {
            this.document.setBody(this.startDatePicker.getValue(), this.endDatePicker.getValue());
        }
        this.applicationFormEvent.updateBody();
    }
}