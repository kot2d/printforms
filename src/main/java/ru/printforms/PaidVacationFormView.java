package ru.printforms;

import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.spring.annotation.UIScope;
import org.springframework.beans.factory.annotation.Autowired;
import ru.printforms.domain.SecuredUser;
import ru.printforms.domain.applicationForms.PaidVacationApplicationForm;
import ru.printforms.services.ApplicationFormEvent;
import ru.printforms.services.ApplicationFormLayout;
import ru.printforms.services.vaadin.DateTimePeriodLayout;
import ru.printforms.services.vaadin.FormProperties;
import ru.printforms.services.vaadin.binders.PaidVacationFormBinder;

@Route("paid_vacation_form")
@UIScope
public class PaidVacationFormView extends VerticalLayout {

    PaidVacationApplicationForm document;
    private DateTimePeriodLayout datePeriodLayout = new DateTimePeriodLayout.Builder(FormProperties.INPUT_WIDTH_INT)
            .withDatePeriod()
            .withCheckPeriodOrOneTime("На один день")
            .withDescription("Укажите даты начала и окончания нового графика:")
            .build();

    private DatePicker startDatePicker = datePeriodLayout.getFirstDay();
    private DatePicker endDatePicker = datePeriodLayout.getEndDate();
    private Checkbox ondDayGraphicCheckbox = datePeriodLayout.getPeriodOrOneTime();

    private ApplicationFormLayout<PaidVacationApplicationForm> appFormLayout;
    ApplicationFormEvent applicationFormEvent = new ApplicationFormEvent();

    public PaidVacationFormView(@Autowired SecuredUser user) {
        this.document = new PaidVacationApplicationForm();
        this.setPadding(false);

        PaidVacationFormBinder formBinder =
                new PaidVacationFormBinder(document, new Binder<>(PaidVacationApplicationForm.class));

        this.appFormLayout = new ApplicationFormLayout<>(user, formBinder, this.document, this.applicationFormEvent);

        VerticalLayout inputLayout = this.appFormLayout.getCustomInputLayout();

        formBinder.bindStartDate(this.startDatePicker);
        formBinder.bindEndDate(this.endDatePicker);

        startDatePicker.addValueChangeListener(valueChangeEvent -> {
            endDatePicker.setValue(startDatePicker.getValue().plusDays(13));
            this.buildBody();
        });

        this.ondDayGraphicCheckbox.addValueChangeListener(valueChangeEvent ->{
            if(this.ondDayGraphicCheckbox.getValue()){
                this.endDatePicker.setVisible(false);
                this.buildBody();
            } else {
                this.endDatePicker.setVisible(true);
                this.buildBody();
            }
        });
        endDatePicker.addValueChangeListener(valueChangeEvent -> {
            this.buildBody();
        });
        this.buildBody();
        inputLayout.add(this.datePeriodLayout);
        this.add(appFormLayout);
    }

    private void buildBody(){
        if(this.ondDayGraphicCheckbox.getValue())
            this.document.setBody(this.startDatePicker.getValue());
        else {
            this.document.setBody(this.startDatePicker.getValue(), this.endDatePicker.getValue());
        }
        this.applicationFormEvent.updateBody();
    }
}
