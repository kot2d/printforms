package ru.printforms.services;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import ru.printforms.domain.Company;

import java.util.Collection;

public class CustomUserDetails implements UserDetails {

    private Collection<? extends GrantedAuthority> authorities;

    private String displayName, username, password, department, position;

    private Company company;

    private Boolean enabled;

    private Boolean accountNonExpired;

    private Boolean accountNonLocked;

    private boolean credentialsNonExpired;

    CustomUserDetails(Collection<? extends GrantedAuthority> authorities, String username, String password
            , String displayName, String department, String position, Company company, Boolean enabled
            , Boolean accountNonExpired, Boolean accountNonLocked, boolean credentialsNonExpired){
        this.authorities = authorities;
        this.displayName = displayName;
        this.department = department;
        this.position = position;
        this.company = company;
        this.username = username;
        this.password = password;
        this.enabled = enabled;
        this.accountNonExpired = accountNonExpired;
        this.accountNonLocked = accountNonLocked;
        this.credentialsNonExpired = credentialsNonExpired;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return this.authorities;
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public String getUsername() {
        return this.username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return this.accountNonExpired;
    }

    @Override
    public boolean isAccountNonLocked() {
        return this.accountNonLocked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return this.credentialsNonExpired;
    }

    @Override
    public boolean isEnabled() {
        return this.enabled;
    }

    public String getDisplayName(){
        return this.displayName;
    }

    public String getDepartment() {
        return department;
    }

    public String getPosition() {
        return position;
    }

    public Company getCompany() {
        return company;
    }
}
