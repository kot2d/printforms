package ru.printforms.services;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.data.binder.ValidationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.printforms.MainView;
import ru.printforms.domain.SecuredUser;
import ru.printforms.domain.applicationForms.ApplicationForm;
import ru.printforms.services.vaadin.WarningNotification;
import ru.printforms.services.vaadin.binders.ApplicationFormBinder;
import ru.printforms.services.vaadin.layouts.*;

import java.util.List;

public class ApplicationFormLayout<T extends ApplicationForm> extends VerticalLayout{

    VerticalLayout mainLayout = new VerticalLayout();
    VerticalLayout customInputLayout = new VerticalLayout();
    private static final Logger logger = LoggerFactory.getLogger(InputLayout.class);


    public ApplicationFormLayout(SecuredUser person, ApplicationFormBinder<T> binder, T document, ApplicationFormEvent event) {
        FormPrinter formPrinter = new FormPrinter();
        this.setPadding(false);

        //PreviewLayout should be initialized before InputLayout or event should run all methods in the end
        PreviewLayout previewLayout = new PreviewLayout(event, document);

        InputLayout<T> inputLayout = new InputLayout<>(person, customInputLayout, binder, event);

        FormMainLayout formMainLayout = new FormMainLayout(inputLayout, previewLayout);

        Button homeButton = new Button(new Icon(VaadinIcon.HOME));
        homeButton.addClickListener(buttonClickEvent -> {UI.getCurrent().navigate(MainView.class);});
        HorizontalLayout printLayout = new HorizontalLayout();
        Button printButton = new Button("Печатать");
        printButton.getStyle().set("padding", "2px");
        printButton.setWidth("100px");
        printButton.setHeight("35px");
        printButton.getStyle().set("margin-bottom", "8px");
        printButton.addThemeVariants(ButtonVariant.MATERIAL_CONTAINED, ButtonVariant.MATERIAL_CONTAINED);
        List<FormPrinter.PrinterDevice> printers = formPrinter.getPrinterDevices();
        Select<FormPrinter.PrinterDevice> printerSelect = new Select<>();
        printerSelect.setLabel("Выбрать принтер");
        printerSelect.setItemLabelGenerator(FormPrinter.PrinterDevice::getName);
        printerSelect.setItems(printers);
        printerSelect.setWidth("300px");
        printerSelect.getStyle().set("margin-left", "10px");

        Checkbox browserPrintCheck = new Checkbox();
        browserPrintCheck.setLabel("Печать из браузера.");
        browserPrintCheck.addValueChangeListener(eventCheck -> {
            if(browserPrintCheck.getValue()){
                printerSelect.setEnabled(false);
            } else{
                printerSelect.setEnabled(true);
            }
        });

        printLayout.setVerticalComponentAlignment(FlexComponent.Alignment.END, printButton);
        printLayout.setVerticalComponentAlignment(FlexComponent.Alignment.END, printerSelect);
        printLayout.setVerticalComponentAlignment(Alignment.CENTER, browserPrintCheck);

        printLayout.setMargin(false);
        printLayout.setPadding(false);
        printLayout.setSpacing(false);
        printLayout.add(printButton, printerSelect, browserPrintCheck);

        mainLayout.add(homeButton, formMainLayout, printLayout);
        mainLayout.setWidth("900px");
        mainLayout.setHeight("900px");
        this.add(mainLayout);
        browserPrintCheck.setValue(true);
        this.setHorizontalComponentAlignment(Alignment.CENTER, mainLayout);

        printButton.addClickListener(click -> {
            try {
                binder.writeBean(document);
                if (browserPrintCheck.isEnabled()){
                    UI.getCurrent().navigate(PrintPreview.class
                            , document.getHeader().getToPersonPart() +
                                    "&" + document.getHeader().getFromPersonPart() +
                                    "&" + document.getTitle() +
                                    "&" + document.getBody() +
                                    "&" + document.getSignerLastNameWithInitials() +
                                    "&" + document.getDocumentDate());
                }
                else {
                    if (printerSelect.getValue() == null) {
                        new WarningNotification("Для печати необходимо выбрать принтер."
                                , null).open();
                    } else formPrinter.printWithPrinter(printerSelect.getValue().getPrintService(), document);
                }
            } catch (ValidationException e) {
                logger.error(e.getMessage());
            }
        });
    }

    public VerticalLayout getCustomInputLayout(){
        return this.customInputLayout;
    }

}