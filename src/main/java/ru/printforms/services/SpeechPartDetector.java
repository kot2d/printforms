package ru.printforms.services;

public class SpeechPartDetector implements ISpeechPartDetectorAdapter {

    public boolean isAdjectives(String part) {
        if (part.endsWith("ий") || part.endsWith("ый") || part.endsWith("ой") || part.endsWith("ая")
                || part.endsWith("ое")) {
            return true;
        }
        return false;
    }

    public boolean isNoun(String part) {
        if (this.isAdjectives(part)) {
            return false;
        }
        return true;
    }
}
