package ru.printforms.services;

import org.springframework.ldap.core.DirContextAdapter;
import org.springframework.ldap.core.DirContextOperations;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.ldap.userdetails.InetOrgPerson;
import org.springframework.security.ldap.userdetails.InetOrgPersonContextMapper;
import org.springframework.security.ldap.userdetails.UserDetailsContextMapper;
import ru.printforms.domain.Company;
import ru.printforms.domain.OsposCompany;

import java.util.Collection;

public class AttributesLDAPUserDetailsContextMapper implements UserDetailsContextMapper {

    private InetOrgPersonContextMapper ldapUserDetailsMapper = new InetOrgPersonContextMapper();

    @Override
    public UserDetails mapUserFromContext(DirContextOperations ctx, String userName
            , Collection<? extends GrantedAuthority> auth)
    {
        UserDetails userDetails = ldapUserDetailsMapper.mapUserFromContext(ctx, userName, auth);

        Company company = null;

        if (((InetOrgPerson)userDetails).getDn().contains("OU=OSPOS"))
            company = new OsposCompany();

        String position = ((InetOrgPerson)userDetails).getTitle().replace(" ИС", "");
        return new CustomUserDetails(auth
                                     , userName
                                     , userDetails.getPassword()
                                     , ((InetOrgPerson)userDetails).getDisplayName()
                                     , ((InetOrgPerson)userDetails).getDescription()
                                     , position
                                     , company
                                     , userDetails.isEnabled()
                                     , userDetails.isAccountNonExpired()
                                     , userDetails.isAccountNonLocked()
                                     , userDetails.isCredentialsNonExpired());
    }

    @Override
    public void mapUserToContext(UserDetails userDetails, DirContextAdapter ctx)
    {
        ldapUserDetailsMapper.mapUserToContext(userDetails, ctx);
    }
}
