package ru.printforms.services.vaadin.layouts;

import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import ru.printforms.domain.Person;
import ru.printforms.domain.applicationForms.ApplicationForm;
import ru.printforms.services.ApplicationFormEvent;
import ru.printforms.services.vaadin.FormDatePicker;
import ru.printforms.services.vaadin.binders.ApplicationFormBinder;

import java.time.LocalDate;

public class FooterDataInputLayout<T extends ApplicationForm> extends VerticalLayout {
    private TextField signerLastNameWithInitials = new TextField();
    private DatePicker signDatePicker = new FormDatePicker("Дата подписи");
    private ApplicationFormEvent event;

    public FooterDataInputLayout(Person person, ApplicationFormBinder<T> binder
            , ApplicationFormEvent event){
        this.event = event;
        binder.bindSignDate(signDatePicker);
        binder.bindSignerLastNameWithInitials(signerLastNameWithInitials);
        this.signerLastNameWithInitials.setLabel("ФИО подписанта");
        this.signerLastNameWithInitials.setWidthFull();
        this.signerLastNameWithInitials.addValueChangeListener(valueChangeEvent -> {
            this.event.updateFooter();
        });

        this.signDatePicker.addValueChangeListener(valueChangeEvent -> {
            this.event.updateFooter();
        });

        this.signDatePicker.setValue(LocalDate.now());
        this.signerLastNameWithInitials.setValue(person.getNames().getShortFullName());
        this.add(signDatePicker, signerLastNameWithInitials);
    }
}
