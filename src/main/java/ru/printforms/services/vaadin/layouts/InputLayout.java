package ru.printforms.services.vaadin.layouts;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.spring.annotation.UIScope;

import ru.printforms.domain.SecuredUser;
import ru.printforms.domain.applicationForms.ApplicationForm;
import ru.printforms.services.ApplicationFormEvent;
import ru.printforms.services.vaadin.FormProperties;
import ru.printforms.services.vaadin.binders.ApplicationFormBinder;

@UIScope
@CssImport("./styles/input_layout_style.css")
public class InputLayout<T extends ApplicationForm> extends VerticalLayout {

    public InputLayout(SecuredUser person, VerticalLayout CustomFormDataInputLayout
            , ApplicationFormBinder<T> binder, ApplicationFormEvent event){

        this.setClassName("input_layout");
        this.setHeightFull();
        this.setWidth(FormProperties.INPUT_WIDTH);
        this.setSpacing(false);
        this.setPadding(false);
        this.setMargin(false);

        Tab formDataInputTab = new Tab("Содержание");
        Tab headerDataInputTab = new Tab("Шапка");
        Tabs tabs = new Tabs(formDataInputTab, headerDataInputTab);
        tabs.setWidthFull();
        tabs.setSelectedTab(formDataInputTab);

        HeaderDataInputLayout<T> headerDataInputLayout = new HeaderDataInputLayout<>(person, binder, event);
        headerDataInputLayout.setVisible(false);

        VerticalLayout dataInputGroupLayout = new VerticalLayout();
        dataInputGroupLayout.setWidth(FormProperties.INPUT_WIDTH);
        dataInputGroupLayout.setSpacing(false);
        dataInputGroupLayout.setPadding(false);
        dataInputGroupLayout.setMargin(false);

        FooterDataInputLayout<T> footerDataInputLayout = new FooterDataInputLayout<>(person.getPerson(), binder, event);

        CustomFormDataInputLayout.setWidth(FormProperties.INPUT_WIDTH);
        CustomFormDataInputLayout.getStyle().set("padding", FormProperties.FORM_PADDING);
        CustomFormDataInputLayout.setSpacing(false);
        dataInputGroupLayout.add(CustomFormDataInputLayout, footerDataInputLayout);

        Map<Tab, Component> tabsToPages = new HashMap<>();
        tabsToPages.put(formDataInputTab, dataInputGroupLayout);
        tabsToPages.put(headerDataInputTab, headerDataInputLayout);
        Set<Component> pagesShown = Stream.of(dataInputGroupLayout)
                .collect(Collectors.toSet());
        tabs.addSelectedChangeListener(tab_event -> {
            pagesShown.forEach(page -> page.setVisible(false));
            pagesShown.clear();
            Component selectedPage = tabsToPages.get(tabs.getSelectedTab());
            selectedPage.setVisible(true);
            pagesShown.add(selectedPage);
        });
        tabs.setSelectedTab(formDataInputTab);

        this.add(tabs, dataInputGroupLayout, headerDataInputLayout);
    }
}
