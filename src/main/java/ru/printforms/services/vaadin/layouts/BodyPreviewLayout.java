package ru.printforms.services.vaadin.layouts;

import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.spring.annotation.UIScope;
import ru.printforms.domain.applicationForms.ApplicationForm;
import ru.printforms.services.ApplicationFormListener;

@UIScope
public class BodyPreviewLayout extends VerticalLayout implements ApplicationFormListener {

    private Span bodyText = new Span();
    private ApplicationForm document;

    public BodyPreviewLayout(ApplicationForm document){
        this.document = document;
        Span bodyTitle = new Span(document.getTitle());
        this.bodyText.getStyle().set("text-indent", "30px");
        this.getElement().getStyle().set("padding", "1em 0 1em 0");
        this.setWidthFull();
        this.setHorizontalComponentAlignment(Alignment.CENTER, bodyTitle);
        this.add(bodyTitle, bodyText);
    }

    public void updateHeader(){};

    public void updateBody(){
        this.bodyText.setText(this.document.getBody());
    };

    public void updateFooter(){};

    public void setBody(String bodyText){
        this.bodyText.setText(bodyText);
    }
}
