package ru.printforms.services.vaadin.layouts;

import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.spring.annotation.UIScope;
import ru.printforms.domain.applicationForms.ApplicationForm;
import ru.printforms.services.ApplicationFormListener;
import ru.printforms.services.utills.FormDateTimeFormatter;

@UIScope
public class FooterPreviewLayout extends VerticalLayout implements ApplicationFormListener {

    private Span signerLastNameWithInitials = new Span();
    private Span signDate = new Span(FormDateTimeFormatter.getEmptyDate());
    private Span signLine = new Span("____________________");
    private ApplicationForm document;

    public FooterPreviewLayout(ApplicationForm document){
        this.document = document;
        this.setWidthFull();
        this.setSpacing(false);
        this.setHorizontalComponentAlignment(FlexComponent.Alignment.END, signDate);
        this.setHorizontalComponentAlignment(FlexComponent.Alignment.END, signerLastNameWithInitials);
        this.setHorizontalComponentAlignment(FlexComponent.Alignment.END, signLine);
        add(signDate, signerLastNameWithInitials, signLine);
    }

    public void setSignerLastNameWithInitials(String signerFioText) {
        this.signerLastNameWithInitials.setText(signerFioText);
    }

    public void updateHeader() {

    }

    public void updateBody() {

    }

    public void updateFooter() {
        this.signDate.setText((this.document.getDocumentDate()==null) ? FormDateTimeFormatter.getEmptyTime() : FormDateTimeFormatter.getDate(this.document.getDocumentDate()));
        this.signerLastNameWithInitials.setText(this.document.getSignerLastNameWithInitials());
    }

    public void setSignDate(String signDate) {
        this.signDate.setText(signDate);
    }
}
