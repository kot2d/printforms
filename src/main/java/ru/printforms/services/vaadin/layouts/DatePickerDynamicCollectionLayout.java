package ru.printforms.services.vaadin.layouts;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import ru.printforms.services.vaadin.FormDatePicker;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

public class DatePickerDynamicCollectionLayout extends VerticalLayout {

    VerticalLayout dateContainer = new VerticalLayout();
    FormDatePicker weekendDatePicker = new FormDatePicker("Выходной день");
    Button addDateButton = new Button(new Icon(VaadinIcon.PLUS_CIRCLE));
    private DatePickerRemovable newDatePickerRemovable;

    public DatePickerDynamicCollectionLayout(){
        this.setMargin(false);
        this.setPadding(false);
        this.setSpacing(false);

        this.dateContainer.setMargin(false);
        this.dateContainer.setPadding(false);
        this.dateContainer.setSpacing(false);
        this.dateContainer.add(new DatePickerRemovable(weekendDatePicker));

        this.addDateButton.addClickListener(click -> {
            int childCount = dateContainer.getComponentCount();
            if(childCount<7) {
                FormDatePicker datePicker = new FormDatePicker("Выходной день");
                Button removeButton = new Button(new Icon(VaadinIcon.MINUS_CIRCLE_O));
                DatePickerRemovable datePickerRemovable = new DatePickerRemovable(datePicker, removeButton);
                removeButton.addClickListener(clickEvent -> {
                    this.dateContainer.remove(datePickerRemovable);
                    addDateButton.setEnabled(true);
                });
                this.newDatePickerRemovable = datePickerRemovable;
                this.dateContainer.addComponentAtIndex(childCount, datePickerRemovable);
            } else addDateButton.setEnabled(false);
        });
        this.add(this.dateContainer, this.addDateButton);
    }

    public void addDate(){
        int childCount = dateContainer.getComponentCount();
        if(childCount<7) {
            FormDatePicker datePicker = new FormDatePicker("Выходной день");
            Button removeButton = new Button(new Icon(VaadinIcon.MINUS_CIRCLE_O));
            DatePickerRemovable datePickerRemovable = new DatePickerRemovable(datePicker, removeButton);
            removeButton.addClickListener(clickEvent -> {
                this.dateContainer.remove(datePickerRemovable);
                addDateButton.setEnabled(true);
            });
            this.newDatePickerRemovable = datePickerRemovable;
            this.dateContainer.addComponentAtIndex(childCount, datePickerRemovable);
        } else addDateButton.setEnabled(false);
    }

    public Button getAddDateButton(){
        return this.addDateButton;
    }

    public DatePicker getNewDatePicker(){
        return this.newDatePickerRemovable.getDatePicker();
    }

    public Button getNewRemoveButton(){
        return this.newDatePickerRemovable.getRemoveButton();
    }

    public DatePicker getStaticDatePicker(){
        return this.weekendDatePicker;
    }

    public List<LocalDate> getValues(){
        return dateContainer.getChildren()
                .filter(child -> ((DatePickerRemovable)child).getValue() != null)
                .map(child -> ((DatePickerRemovable)child).getValue())
                .collect(Collectors.toList());
    }

    class DatePickerRemovable extends HorizontalLayout {
        private DatePicker datePicker;
        private Button removeButton;
        public DatePickerRemovable(DatePicker datePicker, Button button){
            this.datePicker = datePicker;
            this.removeButton = button;
            this.add(datePicker, button);
        }

        public DatePickerRemovable(DatePicker datePicker){
            this.datePicker = datePicker;
            this.add(datePicker);
        }

        public DatePicker getDatePicker() {
            return datePicker;
        }

        public Button getRemoveButton() {
            return removeButton;
        }

        public LocalDate getValue(){
            return this.datePicker.getValue();
        }

    }
}
