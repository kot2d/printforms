package ru.printforms.services.vaadin.layouts;

import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.textfield.TextField;
import ru.printforms.domain.Person;
import ru.printforms.domain.SecuredUser;
import ru.printforms.domain.applicationForms.ApplicationForm;
import ru.printforms.services.ApplicationFormEvent;
import ru.printforms.services.vaadin.FormProperties;
import ru.printforms.services.vaadin.binders.ApplicationFormBinder;

import java.util.ArrayList;
import java.util.List;

public class HeaderDataInputLayout<T extends ApplicationForm> extends VerticalLayout{
    Select<String> chiefSelect = new Select<>();
    private TextArea toPersonPosition = new TextArea();
    private TextField toPersonFullName = new TextField();
    private TextField companyName = new TextField();
    private TextArea fromPersonPosition = new TextArea();
    private TextArea fromPersonDepartment = new TextArea();
    private TextField fromPersonFullName = new TextField();
    private ApplicationFormEvent applicationFormEvent;

    public HeaderDataInputLayout(SecuredUser person, ApplicationFormBinder<T> binder, ApplicationFormEvent applicationFormEvent){
        this.setWidth(FormProperties.INPUT_WIDTH);
        this.applicationFormEvent = applicationFormEvent;
        List<Person> chiefs = new ArrayList<>();
        chiefs.add(person.getCompany().getLocalChief());
        chiefs.add(person.getCompany().getGlobalChief());
        binder.bindFromPersonPosition(this.fromPersonPosition);
        binder.bindFromPersonDepartment(this.fromPersonDepartment);
        binder.bindFromPersonLastNameWithInitials(this.fromPersonFullName);
        binder.bindToPersonLastNameWithInitials(this.toPersonFullName);
        binder.bindToPersonPosition(this.toPersonPosition);
        binder.bindCompanyName(this.companyName);

        this.chiefSelect.setItems(person.getCompany().getLocalChief().getNames().getShortFullName()
                , person.getCompany().getGlobalChief().getNames().getShortFullName());

        this.chiefSelect.addValueChangeListener(event -> {
            Person currentChief = chiefs.stream()
                    .filter(chief -> chiefSelect.getValue().equals(chief.getNames().getShortFullName()))
                    .findAny()
                    .orElse(null);
            this.toPersonPosition.setValue(currentChief.getPosition().getPositionNameInDativeCase());
            this.toPersonFullName.setValue(currentChief.getNames().getFullNameInitialsInDativeCase());
            }
        );

        this.chiefSelect.setValue(person.getCompany().getLocalChief().getNames().getShortFullName());

        this.toPersonPosition.setLabel("Должность начальника в дат.падеже");
        this.toPersonPosition.setWidthFull();
        this.toPersonPosition.addValueChangeListener(valueChangeEvent -> {
            this.applicationFormEvent.updateHeader();
        });
        this.toPersonFullName.setLabel("ФИО начальника в дат.падеже");
        this.toPersonFullName.setWidthFull();
        this.toPersonFullName.addValueChangeListener(valueChangeEvent -> {
            this.applicationFormEvent.updateHeader();
        });
        this.companyName.setLabel("Наименование организации");
        this.companyName.setWidthFull();
        this.companyName.addValueChangeListener(valueChangeEvent -> {
            this.applicationFormEvent.updateHeader();
        });
        this.fromPersonPosition.setLabel("Должность в род.падеже");
        this.fromPersonPosition.setWidthFull();
        this.fromPersonPosition.addValueChangeListener(valueChangeEvent -> {
            this.applicationFormEvent.updateHeader();
        });
        this.fromPersonDepartment.setLabel("Подразделение в род.падеже");
        this.fromPersonDepartment.setWidthFull();
        this.fromPersonDepartment.addValueChangeListener(valueChangeEvent -> {
            this.applicationFormEvent.updateHeader();
        });
        this.fromPersonFullName.setLabel("ФИО в род.падеже");
        this.fromPersonFullName.setWidthFull();
        this.fromPersonFullName.addValueChangeListener(valueChangeEvent -> {
            this.applicationFormEvent.updateHeader();
        });

        this.fromPersonPosition.setValue(person.getPerson().getPosition().getPositionNameInGenitiveCase());
        this.fromPersonDepartment.setValue(person.getPerson().getPosition().getDepartmentNameInGenitiveCase());
        this.fromPersonFullName.setValue(person.getPerson().getNames().getFullNameInitialsInGenitiveCase());
        this.companyName.setValue(person.getCompany().getFullName());

        this.add(chiefSelect, toPersonPosition, companyName, toPersonFullName, fromPersonPosition, fromPersonDepartment
                , fromPersonFullName);
    }
}
