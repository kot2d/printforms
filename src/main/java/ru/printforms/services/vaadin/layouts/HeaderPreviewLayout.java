package ru.printforms.services.vaadin.layouts;

import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.spring.annotation.UIScope;
import ru.printforms.domain.applicationForms.ApplicationForm;
import ru.printforms.services.ApplicationFormListener;

@UIScope
public class HeaderPreviewLayout extends VerticalLayout implements ApplicationFormListener {

    private Span toPersonPart = new Span();
    private Span fromPersonPart = new Span();
    ApplicationForm document;

    public HeaderPreviewLayout(ApplicationForm document){
        this.document = document;
        this.setWidth("250px");
        this.setSpacing(false);
        this.fromPersonPart.setWidthFull();
        this.toPersonPart.setWidthFull();
        this.add(toPersonPart, fromPersonPart);
    }

    public void updateHeader(){
        this.toPersonPart.setText(this.document.getHeader().getToPersonPart());
        this.fromPersonPart.setText(this.document.getHeader().getFromPersonPart());
    };

    public void updateBody(){
    };

    public void updateFooter(){};
}
