package ru.printforms.services.vaadin.layouts;

import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.spring.annotation.UIScope;

@UIScope
public class FormMainLayout extends HorizontalLayout {
    public FormMainLayout(Component inputLayout, Component previewLayout){
        setWidthFull();
        setHeightFull();
        add(inputLayout, previewLayout);
    }
}
