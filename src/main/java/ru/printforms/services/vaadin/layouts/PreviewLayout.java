package ru.printforms.services.vaadin.layouts;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.spring.annotation.UIScope;
import ru.printforms.domain.applicationForms.ApplicationForm;
import ru.printforms.services.ApplicationFormEvent;
import ru.printforms.services.vaadin.FormProperties;

@UIScope
@CssImport("./styles/preview_layout_style.css")
public class PreviewLayout extends VerticalLayout {

    private HeaderPreviewLayout headerPreviewLayout;
    private BodyPreviewLayout bodyPreviewLayout;
    private FooterPreviewLayout footerPreviewLayout;

    public PreviewLayout (Component header, Component body, Component footer){
        this.setWidth(FormProperties.PREVIEW_WIDTH);
        this.setPadding(false);
        this.setHorizontalComponentAlignment(Alignment.END, header);
        this.setClassName("preview_layout");
        Div divider = new Div();
        divider.setHeight("100px");
        this.setHorizontalComponentAlignment(Alignment.CENTER, body);
        this.setHorizontalComponentAlignment(Alignment.END, footer);
        this.add(header, divider, body, footer);
    }

    public PreviewLayout (ApplicationFormEvent event, ApplicationForm document){
        this.setWidth(FormProperties.PREVIEW_WIDTH);
        this.setPadding(false);

        this.headerPreviewLayout = new HeaderPreviewLayout(document);
        this.bodyPreviewLayout = new BodyPreviewLayout(document);
        this.footerPreviewLayout = new FooterPreviewLayout(document);

        event.addListener(this.bodyPreviewLayout);
        event.addListener(this.headerPreviewLayout);
        event.addListener(this.footerPreviewLayout);

        this.setHorizontalComponentAlignment(Alignment.END, headerPreviewLayout);
        this.setClassName("preview_layout");
        Div divider = new Div();
        divider.setHeight("100px");
        this.setHorizontalComponentAlignment(Alignment.CENTER, bodyPreviewLayout);
        this.setHorizontalComponentAlignment(Alignment.END, footerPreviewLayout);
        this.add(headerPreviewLayout, divider, bodyPreviewLayout, footerPreviewLayout);
    }
}
