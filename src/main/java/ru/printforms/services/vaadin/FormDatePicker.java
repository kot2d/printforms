package ru.printforms.services.vaadin;

import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.spring.annotation.UIScope;

import java.util.Arrays;
import java.util.Locale;

@UIScope
public class FormDatePicker extends DatePicker {
    public FormDatePicker(String label){
        this.setLabel(label);
        this.setLocale(Locale.UK);
        DatePicker.DatePickerI18n datePickerI18n = new DatePicker.DatePickerI18n();
        datePickerI18n.setFirstDayOfWeek(1);
        datePickerI18n.setWeek("Неделя");
        datePickerI18n.setCalendar("Календарь");
        datePickerI18n.setClear("Очистить");
        datePickerI18n.setToday("Сегодня");
        datePickerI18n.setCancel("Отмена");
        datePickerI18n.setWeekdays(Arrays.asList("Воскресенье", "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота"));
        datePickerI18n.setWeekdaysShort(Arrays.asList("Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"));
        datePickerI18n.setMonthNames(Arrays.asList("Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август",
                "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"));
        this.setI18n(datePickerI18n);
    }
}
