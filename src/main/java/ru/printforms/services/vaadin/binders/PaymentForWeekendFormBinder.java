package ru.printforms.services.vaadin.binders;

import com.vaadin.flow.component.HasValue;
import com.vaadin.flow.data.binder.Binder;
import ru.printforms.domain.applicationForms.PaymentForWeekendApplicationForm;

import java.time.LocalDate;

public class PaymentForWeekendFormBinder extends ApplicationFormBinder<PaymentForWeekendApplicationForm> {
    Binder<PaymentForWeekendApplicationForm> binder;

    public PaymentForWeekendFormBinder(PaymentForWeekendApplicationForm document, Binder<PaymentForWeekendApplicationForm> binder){
        super(document, binder);
        this.binder = binder;
        this.binder.setBean(document);
    }
    public void bindWeekendDate(HasValue<?, LocalDate> weekendDate) {
        binder.forField(weekendDate)
                .asRequired()
                .bind(PaymentForWeekendApplicationForm::getWeekendDate, PaymentForWeekendApplicationForm::setWeekendDate);
    }
}
