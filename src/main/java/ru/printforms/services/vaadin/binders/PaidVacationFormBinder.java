package ru.printforms.services.vaadin.binders;

import com.vaadin.flow.component.HasValue;
import com.vaadin.flow.data.binder.Binder;
import ru.printforms.domain.applicationForms.PaidVacationApplicationForm;

import java.time.LocalDate;

public class PaidVacationFormBinder extends ApplicationFormBinder<PaidVacationApplicationForm> {

    Binder<PaidVacationApplicationForm> binder;

    public PaidVacationFormBinder(PaidVacationApplicationForm document, Binder<PaidVacationApplicationForm> binder){
        super(document, binder);
        this.binder = binder;
        this.binder.setBean(document);
    }

    public void bindStartDate(HasValue<?, LocalDate> startDate) {
        binder.forField(startDate)
              .asRequired()
              .bind(PaidVacationApplicationForm::getStartDate, PaidVacationApplicationForm::setStartDate);
    }
    public void bindEndDate(HasValue<?, LocalDate> endDate) {
        binder.forField(endDate)
                .asRequired()
                .bind(PaidVacationApplicationForm::getEndDate, PaidVacationApplicationForm::setEndDate);
    }
}
