package ru.printforms.services.vaadin.binders;

import com.vaadin.flow.component.HasValue;
import com.vaadin.flow.data.binder.Binder;
import ru.printforms.domain.Messages;
import ru.printforms.domain.applicationForms.UnpaidVacationApplicationForm;

import java.time.LocalDate;

public class UnpaidVacationFormBinder extends ApplicationFormBinder<UnpaidVacationApplicationForm> {

    private final Binder<UnpaidVacationApplicationForm> binder;

    public UnpaidVacationFormBinder(UnpaidVacationApplicationForm document, Binder<UnpaidVacationApplicationForm> binder){
        super(document, binder);
        this.binder = binder;
        this.binder.setBean(document);
    }

    public void bindStartDate(HasValue<?, LocalDate> startDate) {
        binder.forField(startDate)
                .asRequired()
                .bind(UnpaidVacationApplicationForm::getStartDate, UnpaidVacationApplicationForm::setStartDate);
    }
    public void bindEndDate(HasValue<?, LocalDate> endDate, HasValue<?, LocalDate> startDate) {
        binder.forField(endDate)
                .asRequired()
                .withValidator(value -> value.isAfter(startDate.getValue()), Messages.START_DATE_EARLIER_END_DATE)
                .bind(UnpaidVacationApplicationForm::getEndDate, UnpaidVacationApplicationForm::setEndDate);
    }
}