package ru.printforms.services.vaadin.binders;

import com.vaadin.flow.component.HasValue;
import com.vaadin.flow.data.binder.Binder;
import ru.printforms.domain.Messages;
import ru.printforms.domain.applicationForms.VacationForWeekendApplicationForm;

import java.time.LocalDate;

public class VacationDayForWeekendWorkBinder extends ApplicationFormBinder<VacationForWeekendApplicationForm> {

    VacationForWeekendApplicationForm document;
    Binder<VacationForWeekendApplicationForm> binder;

    public VacationDayForWeekendWorkBinder(VacationForWeekendApplicationForm document, Binder<VacationForWeekendApplicationForm> binder){
        super(document, binder);
        this.document = document;
        this.binder = binder;
        this.binder.setBean(this.document);
    }

    public void bindVacationDate(HasValue<?, LocalDate> vacationDate) {
        binder.forField(vacationDate)
                .withValidator(value -> !value.toString().isEmpty(), Messages.EMPTY_FIELD)
                .bind(VacationForWeekendApplicationForm::getVacationDay
                        , VacationForWeekendApplicationForm::setVacationDay);
    }

    public void bindWeekendDate(HasValue<?, LocalDate> weekendDate) {
        binder.forField(weekendDate)
                .withValidator(value -> !value.toString().isEmpty(), Messages.EMPTY_FIELD)
                .bind(VacationForWeekendApplicationForm::getWeekendDay
                        , VacationForWeekendApplicationForm::setWeekendDay);
    }
}
