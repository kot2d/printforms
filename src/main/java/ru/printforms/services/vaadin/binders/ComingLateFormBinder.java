package ru.printforms.services.vaadin.binders;

import com.vaadin.flow.component.HasValue;
import com.vaadin.flow.data.binder.Binder;
import ru.printforms.domain.Messages;
import ru.printforms.domain.applicationForms.ComingLateApplicationForm;

import java.time.LocalDate;
import java.time.LocalTime;

public class ComingLateFormBinder extends ApplicationFormBinder<ComingLateApplicationForm> {

    private final Binder<ComingLateApplicationForm> binder;

    public ComingLateFormBinder(ComingLateApplicationForm document, Binder<ComingLateApplicationForm> binder){
        super(document, binder);
        this.binder = binder;
        this.binder.setBean(document);
    }

    public void bindStartTime(HasValue<?, LocalTime> startTime) {
        binder.forField(startTime)
                .asRequired()
                .bind(ComingLateApplicationForm::getStartTime, ComingLateApplicationForm::setStartTime);
    }

    public void bindEndTime(HasValue<?, LocalTime> endTime, HasValue<?, LocalTime> startTime) {
        binder.forField(endTime)
                .asRequired()
                .withValidator(value -> value.isAfter(startTime.getValue()), Messages.START_TIME_EARLIER_END_TIME)
                .bind(ComingLateApplicationForm::getEndTime, ComingLateApplicationForm::setEndTime);
    }

    public void bindDate(HasValue<?, LocalDate> date) {
        binder.forField(date)
                .asRequired()
                .bind(ComingLateApplicationForm::getDate, ComingLateApplicationForm::setDate);
    }
}
