package ru.printforms.services.vaadin.binders;

import com.vaadin.flow.component.HasValue;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.ValidationException;
import ru.printforms.domain.applicationForms.ApplicationForm;

import java.time.LocalDate;

public class ApplicationFormBinder<T extends ApplicationForm>{

    protected Binder<T> binder;
    ApplicationForm document;

    public ApplicationFormBinder(T document, Binder<T> binder){
        this.binder = binder;
        this.document = document;
    }

    public void bindToPersonPosition(TextArea toPersonPosition) {
        this.binder.forField(toPersonPosition)
                .asRequired()
                .bind(ApplicationForm::getToPersonPosition, ApplicationForm::setToPersonPosition);
    }

    public void bindCompanyName(TextField companyName) {
        this.binder.forField(companyName)
                .asRequired()
                .bind(ApplicationForm::getCompanyName, ApplicationForm::setCompanyName);
    }

    public void bindToPersonLastNameWithInitials(TextField toPersonLastNameWithInitials) {
        this.binder.forField(toPersonLastNameWithInitials)
                .asRequired()
                .bind(ApplicationForm::getToPersonLastNameWithInitials, ApplicationForm::setToPersonLastNameWithInitials);
    }

    public void bindFromPersonPosition(TextArea fromPersonPosition) {
        this.binder.forField(fromPersonPosition)
                .asRequired()
                .bind(ApplicationForm::getFromPersonPosition, ApplicationForm::setFromPersonPosition);
    }

    public void bindFromPersonDepartment(TextArea fromPersonDepartment) {
        this.binder.forField(fromPersonDepartment)
                .asRequired()
                .bind(ApplicationForm::getFromPersonDepartment, ApplicationForm::setFromPersonDepartment);
    }

    public void bindFromPersonLastNameWithInitials(TextField fromPersonLastNameWithInitials) {
        this.binder.forField(fromPersonLastNameWithInitials)
                .asRequired()
                .bind(ApplicationForm::getFromPersonLastNameWithInitials, ApplicationForm::setFromPersonLastNameWithInitials);
    }

    public void bindSignerLastNameWithInitials(TextField SignerLastNameWithInitials) {
        this.binder.forField(SignerLastNameWithInitials)
                .asRequired()
                .bind(ApplicationForm::getSignerLastNameWithInitials, ApplicationForm::setSignerLastNameWithInitials);
    }

    public void bindSignDate(HasValue<?, LocalDate> signDate) {
        this.binder.forField(signDate)
                .asRequired()
                .bind(ApplicationForm::getDocumentDate, ApplicationForm::setDocumentDate);
    }

    public void writeBean(T document) throws ValidationException {
        this.binder.writeBean(document);
    }

    public ApplicationForm getDocument(){
        return this.document;
    }
}
