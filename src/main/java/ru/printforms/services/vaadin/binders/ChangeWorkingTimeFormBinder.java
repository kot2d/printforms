package ru.printforms.services.vaadin.binders;


import com.vaadin.flow.component.HasValue;
import com.vaadin.flow.data.binder.Binder;
import ru.printforms.domain.applicationForms.ChangeWorkingTimeApplicationForm;

import java.time.LocalDate;
import java.time.LocalTime;

public class ChangeWorkingTimeFormBinder extends ApplicationFormBinder<ChangeWorkingTimeApplicationForm> {
    Binder<ChangeWorkingTimeApplicationForm> binder;
    public ChangeWorkingTimeFormBinder(ChangeWorkingTimeApplicationForm document, Binder<ChangeWorkingTimeApplicationForm> binder){
        super(document, binder);
        this.binder = binder;
        this.binder.setBean(document);
    }

    public void bindStartTime(HasValue<?, LocalTime> startTime) {
        binder.forField(startTime)
                .asRequired()
                .bind(ChangeWorkingTimeApplicationForm::getStartTime, ChangeWorkingTimeApplicationForm::setStartTime);
    }

    public void bindEndTime(HasValue<?, LocalTime> endTime, HasValue<?, LocalTime> startTime) {
        binder.forField(endTime)
                .asRequired()
                .bind(ChangeWorkingTimeApplicationForm::getEndTime, ChangeWorkingTimeApplicationForm::setEndTime);
    }

    public void bindStartDate(HasValue<?, LocalDate> startDate) {
        binder.forField(startDate)
                .asRequired()
                .bind(ChangeWorkingTimeApplicationForm::getStartDate, ChangeWorkingTimeApplicationForm::setStartDate);
    }

    public void bindEndDate(HasValue<?, LocalDate> endDate, HasValue<?, LocalDate> startDate) {
        binder.forField(endDate)
                .asRequired()
                .bind(ChangeWorkingTimeApplicationForm::getEndDate, ChangeWorkingTimeApplicationForm::setEndDate);
    }

    public void bindOneDayGraphic(HasValue<?, Boolean> oneDayGraphic) {
        binder.forField(oneDayGraphic)
                .asRequired()
                .bind(ChangeWorkingTimeApplicationForm::getOneDayGraphic, ChangeWorkingTimeApplicationForm::setOneDayGraphic);
    }
}
