package ru.printforms.services.vaadin;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;

public class WarningNotification{

    private Notification notification;

    public WarningNotification(String messageText, String buttonText){
        Label message = new Label(messageText);
        VerticalLayout messageLayout = new VerticalLayout();
        this.notification = new Notification(messageLayout);
        this.notification.setPosition(Notification.Position.TOP_CENTER);
        messageLayout.setHorizontalComponentAlignment(FlexComponent.Alignment.CENTER, message);
        if(buttonText != null) {
            Button messageCloseButton = new Button(buttonText);
            messageLayout.add(message, messageCloseButton);
            messageLayout.setHorizontalComponentAlignment(FlexComponent.Alignment.CENTER, messageCloseButton);
            messageCloseButton.addClickListener(event -> notification.close());
        } else{
            messageLayout.add(message);
            this.notification.setDuration(1500);
        }
    }

    public void open(){
        notification.open();
    }
}
