package ru.printforms.services.vaadin;

public class FormProperties {
    public final static int INPUT_WIDTH_INT = 300;
    public final static String INPUT_WIDTH = INPUT_WIDTH_INT + "px";
    public final static String PREVIEW_WIDTH = "600px";
    public final static String PREVIEW_HEADER_WIDTH = "250px";
    public final static String FORM_WIDTH = "900px";
    public final static String FORM_HEIGHT = "900px";
    public final static int FORM_PADDING_INT = 10;
    public final static String FORM_PADDING = FORM_PADDING_INT + "px";
}
