package ru.printforms.services.vaadin;

import com.vaadin.flow.component.timepicker.TimePicker;
import com.vaadin.flow.spring.annotation.UIScope;

import java.time.Duration;
import java.util.Locale;
import java.util.Objects;

@UIScope
public class FormTimePicker extends TimePicker {

    public FormTimePicker(String label, String min, String max, long step){
        this.setLabel(label);
        this.setLocale(Locale.FRANCE);
        int width = FormProperties.INPUT_WIDTH_INT / 2 -
                FormProperties.FORM_PADDING_INT * 2;
        this.setWidth(width + "px");
        if(Objects.nonNull(min)) this.setMin(min);
        if(Objects.nonNull(max)) this.setMax(max);
        if(step>0) this.setStep(Duration.ofMinutes(step));
    }
    public FormTimePicker(String label, String min, String max){
        this(label, min, max, 0);
    }

}
