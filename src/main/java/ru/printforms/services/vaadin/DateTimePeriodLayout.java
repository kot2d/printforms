package ru.printforms.services.vaadin;

import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.timepicker.TimePicker;


public class DateTimePeriodLayout extends VerticalLayout {

    private Span label = new Span();
    private HorizontalLayout periodLayout = new HorizontalLayout();
    private DatePicker firstDay;
    private DatePicker endDate;
    private TimePicker startTime;
    private TimePicker endTime;
    private Checkbox periodOrOneTime;
    private int dateTimePickerWidth;

    private DateTimePeriodLayout(int width){
        this.setWidth(width + "px");
        this.setSpacing(false);
        this.setMargin(false);
        this.setPadding(false);
        this.periodLayout.setWidth(width + "px");
        this.label.setWidthFull();
        dateTimePickerWidth = width / 2 - FormProperties.FORM_PADDING_INT * 2;
    }

    private void construct(){
        if(this.periodOrOneTime == null)this.add(this.label, this.periodLayout);
        else this.add(this.label, this.periodOrOneTime, this.periodLayout);
    }

    public DatePicker getFirstDay() {
        return firstDay;
    }

    public DatePicker getEndDate() {
        return endDate;
    }

    public TimePicker getStartTime() {
        return startTime;
    }

    public TimePicker getEndTime() {
        return endTime;
    }

    public Checkbox getPeriodOrOneTime() {
        return periodOrOneTime;
    }

    public static class Builder{

        private DateTimePeriodLayout layout;

        public Builder(int width){
            layout = new DateTimePeriodLayout(width);
        }

        public Builder withDescription(String description){
            this.layout.label.setText(description);
            return this;
        }

        public Builder withTimePeriod(){
            this.layout.startTime = new FormTimePicker("Приход", "07:00", "15:00");
            this.layout.startTime.setWidth(this.layout.dateTimePickerWidth + "px");
            this.layout.endTime = new FormTimePicker("Уход", "12:30", "23:30", 30);
            this.layout.endTime.setWidth(this.layout.dateTimePickerWidth + "px");
            this.layout.periodLayout.add(this.layout.startTime, this.layout.endTime);
            return this;
        }

        public Builder withDatePeriod(){
            this.layout.firstDay = new FormDatePicker("Начало");
            this.layout.firstDay.setWidth(this.layout.dateTimePickerWidth + "px");
            this.layout.endDate = new FormDatePicker("Окончание");
            this.layout.endDate.setWidth(this.layout.dateTimePickerWidth + "px");
            this.layout.periodLayout.add(this.layout.firstDay, this.layout.endDate);
            return this;
        }

        public Builder withCheckPeriodOrOneTime(String label){
            this.layout.periodOrOneTime = new Checkbox(label);
            return this;
        }

        public DateTimePeriodLayout build(){
            this.layout.construct();
            return this.layout;
        }
    }
}
