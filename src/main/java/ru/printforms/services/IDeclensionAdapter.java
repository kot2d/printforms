package ru.printforms.services;

public interface IDeclensionAdapter {
    String getNounInGenitive(String noun);
    String getLastNameInGenitive(String lastName);
    String getFirstNameInGenitive(String firstName);
    String getMiddleNameInGenitive(String middleName);
    String getAdjectiveInGenitive(String adjective);
}
