package ru.printforms.services;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.printforms.domain.applicationForms.ApplicationForm;
import ru.printforms.services.utills.FormDateTimeFormatter;
import ru.printforms.services.utills.TextFormatter;
import java.io.*;


public class PdfDocument {

    private ByteArrayOutputStream outputStream;
    private static final Logger logger = LoggerFactory.getLogger(PdfDocument.class);

    public PdfDocument(ApplicationForm formDocument){
        try {
            BaseFont arsenalBaseFont = BaseFont
                    .createFont("fonts/times-new-roman.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            Font defaultFont = new Font(arsenalBaseFont, 13);
            Document document = new Document();
            document.setMargins(60, 40, 75, 30);

            outputStream = new ByteArrayOutputStream();
            PdfWriter.getInstance(document, outputStream);

            document.open();
            Paragraph constHeader = new Paragraph(TextFormatter.addNonBreakingSpaceToFio(formDocument.getHeader().getToPersonPart())
                    , defaultFont);
            constHeader.setIndentationLeft(340);
            document.add(constHeader);
            Paragraph header = new Paragraph(formDocument.getHeader().getFromPersonPart(), defaultFont);
            header.setIndentationLeft(340);
            document.add(header);
            Paragraph dividerHeader = new Paragraph("\n\n\n\n\n\n\n\n\n");
            document.add(dividerHeader);
            Paragraph title = new Paragraph(formDocument.getTitle(), defaultFont);
            title.setAlignment(Element.ALIGN_CENTER);
            document.add(title);
            Paragraph dividerTitle = new Paragraph("\n");
            document.add(dividerTitle);
            Paragraph body = new Paragraph(formDocument.getBody(), defaultFont);
            body.setFirstLineIndent(25);
            body.setAlignment(Element.ALIGN_JUSTIFIED);
            document.add(body);
            Paragraph dividerSignature = new Paragraph("\n\n\n\n");
            document.add(dividerSignature);
            Paragraph signature = new Paragraph(formDocument.getSignerLastNameWithInitials(), defaultFont);
            signature.setSpacingBefore(20);
            signature.setAlignment(Element.ALIGN_RIGHT);
            Paragraph signDate = new Paragraph(FormDateTimeFormatter.getDate(formDocument.getDocumentDate()), defaultFont);
            signDate.setAlignment(Element.ALIGN_RIGHT);
            document.add(signature);
            document.add(signDate);
            document.close();
        }
        catch (DocumentException e){
            logger.error(e.getMessage());
        }
        catch (IOException e){
            logger.error(e.getMessage());
        }
    }

    public ByteArrayOutputStream getOutputStream(){
        return this.outputStream;
    }

}
