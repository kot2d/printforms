package ru.printforms.services;

import java.util.Arrays;
import java.util.List;

public class DeclensionBuilder implements IDeclensionAdapter{

    final static List<String> vowels = Arrays.asList("а", "у", "о", "ы", "и", "э", "я", "ю", "ё", "е");
    final static List<String> consonants = Arrays.asList("б", "в", "г", "д", "ж", "з", "к", "л", "м", "н", "п", "р", "с"
            , "т", "ф", "х", "ц", "ч", "ш", "щ");

    public String getNounInGenitive(String noun){
        List<String> nounGroup1 = Arrays.asList("ебь", "евь", "егь", "едь", "ежь", "езь", "екь", "ель", "емь", "ень"
                , "епь", "ерь", "есь", "еть", "ефь", "ехь", "ець", "ечь");
        if(noun.endsWith("а")){
            return noun.substring(0, noun.length()-1) + "ы";
        } else if(noun.endsWith("я")){
            return  noun.substring(0, noun.length()-1) + "и";
        } else if(noun.endsWith("ь")){
            if(nounGroup1.contains(noun.substring(noun.length() - 3))){
                return noun.substring(0, noun.length()-1) + "я";
            }
            return  noun.substring(0, noun.length()-1) + "и";
        } else if(noun.endsWith("о")){
            return noun.substring(0, noun.length()-1) + "а";
        } else if(noun.endsWith("е") || noun.endsWith("й")){
            return noun.substring(0, noun.length()-1) + "я";
        } else if(consonants.contains(String.valueOf(noun.charAt(noun.length()-1)))) {
            return noun + "a";
        }
        return noun;
    }

    public String getLastNameInGenitive(String lastName) {

        List<String> lastNameGroup1 = Arrays.asList("е", "и", "о", "у", "ы", "э", "ю");
        List<String> lastNameGroup2 = Arrays.asList("ая", "уя", "оя", "ыя", "ия", "яя", "эя", "юя", "ёя", "ея");

        if (lastName.endsWith("ов") || lastName.endsWith("eв")) {
            return lastName + "а";
        } else if (lastName.endsWith("ова") || lastName.endsWith("ева")|| lastName.endsWith("на")) {
            return lastName.substring(0, lastName.length() - 1) + "ой";
        } else if (lastName.endsWith("ин") || lastName.endsWith("ын")) {
            return lastName + "а";
        } else if (lastName.endsWith("ский") || lastName.endsWith("цкий")) {
            return lastName.substring(0, lastName.length() - 2) + "ого";
        } else if (lastName.endsWith("ых") || lastName.endsWith("их")) {
            return lastName;
        } else if (lastNameGroup1.contains(String.valueOf(lastName.charAt(lastName.length() - 1)))) {
            return lastName;
        } else if (lastNameGroup2.contains(lastName.substring(lastName.length() - 2))) {
            return lastName.substring(0, lastName.length() - 2) + "ой";
        } else if (lastName.endsWith("ь")) {
            return lastName;
        }
        return lastName;
    }

    public String getFirstNameInGenitive(String firstName) {
        if (firstName.endsWith("й")) {
            return firstName.substring(0, firstName.length() - 1) + "я";
        } else if (consonants.contains(String.valueOf(firstName.charAt(firstName.length() - 1)))) {
            return firstName + "а";
        } else if (firstName.endsWith("я")) {
            return firstName.substring(0, firstName.length() - 1) + "и";
        } else if (firstName.endsWith("а")) {
            if(firstName.endsWith("ша") || firstName.endsWith("ща") ){
                return firstName.substring(0, firstName.length() - 1) + "и";
            } else return firstName.substring(0, firstName.length() - 1) + "ы";
        }
        return firstName;
    }

    public String getMiddleNameInGenitive(String middleName) {
        if (middleName == null) return "";
        if (middleName.endsWith("ич")) {
            return middleName + "а";
        } else if (middleName.endsWith("вна")) {
            return middleName.substring(0, middleName.length() - 1) + "ы";
        }
        return middleName;
    }

    public String getAdjectiveInGenitive(String adjective){

        if (adjective.endsWith("ый") || adjective.endsWith("ое")) {
            return adjective.substring(0, adjective.length() - 2) + "ого";
        } else if (adjective.endsWith("ий")){
            return adjective.substring(0, adjective.length() - 2) + "его";
        } else if (adjective.endsWith("ой")){
            return adjective.substring(0, adjective.length() - 1) + "ого";
        } else if (adjective.endsWith("ая")){
            return adjective.substring(0, adjective.length() - 2) + "ой";
        }
        return adjective;
    }
}