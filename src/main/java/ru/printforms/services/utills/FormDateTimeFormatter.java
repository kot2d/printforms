package ru.printforms.services.utills;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class FormDateTimeFormatter {

    static private DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
    static private DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("HH:mm");

    public static String getTime(LocalTime dateTime){
        return dateTime.format(timeFormatter);
    }

    public static String getEmptyTime(){
        return "__:__";
    }

    public static String getDate(LocalDate dateTime){
        return dateTime.format(dateFormatter);
    }

    public static String getEmptyDate(){
        return "__.__.____";
    }
}
