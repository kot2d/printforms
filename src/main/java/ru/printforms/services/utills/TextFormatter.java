package ru.printforms.services.utills;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TextFormatter {

    public static String addNonBreakingSpaceToFio(String userHeader){
        Pattern initialsSample1 = Pattern.compile("[а-яА-Я]([а-яА-Я]\\.[а-яА-Я]\\.)$", Pattern.UNICODE_CHARACTER_CLASS);
        Matcher matcher = initialsSample1.matcher(userHeader);
        if(matcher.find()){
            String finalString = "\u00a0" + matcher.group(1);
            finalString = finalString.replaceFirst("\\.", ".\u00a0");
            return userHeader.replace(matcher.group(1), finalString);
        }

        Pattern initialsSample2 = Pattern.compile("\\s[а-яА-Я]\\.[а-яА-Я]\\.$", Pattern.UNICODE_CHARACTER_CLASS);
        matcher = initialsSample2.matcher(userHeader);
        if(matcher.find()){
            String finalString = matcher.group(0).replaceFirst("\\s", "\u00a0");
            finalString = finalString.replaceFirst("\\.", ".\u00a0");
            return userHeader.replace(matcher.group(0), finalString);
        }

        Pattern initialsSample3 = Pattern.compile("\\s[а-яА-Я]\\.\\s[а-яА-Я]\\.$", Pattern.UNICODE_CHARACTER_CLASS);
        matcher = initialsSample3.matcher(userHeader);
        if(matcher.find()) {
            String finalString = matcher.group(0).replace(" ", "\u00a0");
            return userHeader.replace(matcher.group(0), finalString);
        }
        return userHeader;
    }

    public static String replaceSpaceWithNonBreakingSpace(String text){
           return text.replaceFirst(" ", "\u00a0");
    }

    public static String getCalendarDaysDeviation(long daysAmount){
        if(daysAmount==1) return  "календарный день";
        if(daysAmount>1 && daysAmount <5) return  "календарных дня";
        if(daysAmount==21) return  "календарный день";
        if(daysAmount>21 && daysAmount <25) return  "календарных дня";
        if(daysAmount==31) return  "календарный день";
        return "календарных дней";
    }

    public static String getAmountLiterally(long amount){
        if(amount==1) return  "один";
        if(amount==2) return  "два";
        if(amount==3) return  "три";
        if(amount==4) return  "четыре";
        if(amount==5) return  "пять";
        if(amount==6) return  "шесть";
        if(amount==7) return  "семь";
        if(amount==8) return  "восемь";
        if(amount==9) return  "девять";
        if(amount==10) return  "десять";
        if(amount==11) return  "одинадцать";
        if(amount==12) return  "двенадцать";
        if(amount==13) return  "тринадцать";
        if(amount==14) return  "четырнадцать";
        if(amount==15) return  "пятнадцать";
        if(amount==16) return  "шестнадцать";
        if(amount==17) return  "семнадцать";
        if(amount==18) return  "восемнадцать";
        if(amount==19) return  "девятнадцать";
        if(amount==20) return  "двадцать";
        if(amount==21) return  "двадцать один";
        if(amount==22) return  "двадцать два";
        if(amount==23) return  "двадцать три";
        if(amount==24) return  "двадцать четыре";
        if(amount==25) return  "двадцать пять";
        if(amount==26) return  "двадцать шесть";
        if(amount==27) return  "двадцать семь";
        if(amount==28) return  "двадцать восемь";
        if(amount==29) return  "двадцать девять";
        if(amount==30) return  "тридцать";
        if(amount==31) return  "тридцать один";
        return "  ";
    }
}
