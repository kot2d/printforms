package ru.printforms.services.utills;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Component
@Primary
public class TextWrapper implements IStringWrapper {

   public String wrap(String text, int limit){
        String unformatedPart = text;
        StringBuilder sb = new StringBuilder();
        while (unformatedPart.length() > limit){
            int lastSpaceIndex = unformatedPart.substring(0, limit-1).lastIndexOf(' ');
            sb.append(unformatedPart.substring(0, lastSpaceIndex));
            sb.append("\n");
            unformatedPart = unformatedPart.substring(lastSpaceIndex+1);
        }
        sb.append(unformatedPart);
        return sb.toString();
    }

}
