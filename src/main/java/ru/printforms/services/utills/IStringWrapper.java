package ru.printforms.services.utills;

public interface IStringWrapper {

    String wrap(String text, int lineLimit);

}
