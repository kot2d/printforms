package ru.printforms.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import ru.printforms.domain.PersonPosition;

import javax.annotation.PostConstruct;
import java.util.List;

@Component
public class LocalDataCollections {

    @Autowired
    ApplicationContext applicationContext;

    private ExternalFileReader fileReader;

    @PostConstruct
    void init(){
        this.fileReader = new ExternalFileReader(this.applicationContext);
    }

    public PersonPosition getUserPosition(String userName){
        List<String> lines = this.fileReader.read("userPositions.csv");
        for(String fileLine: lines) {
            if (!fileLine.isEmpty()) {
                String[] lineParts = fileLine.split(",");
                if(lineParts[0].equals(userName)){
                    return new PersonPosition(lineParts[1], lineParts[2]);
                }
            }
        }
        return null;
    }
}
