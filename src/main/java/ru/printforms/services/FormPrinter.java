package ru.printforms.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.printforms.domain.applicationForms.ApplicationForm;

import javax.print.*;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.Copies;
import javax.print.attribute.standard.MediaSizeName;
import javax.print.attribute.standard.PrinterName;
import javax.print.attribute.standard.Sides;
import java.awt.print.PrinterJob;
import java.io.*;
import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.List;


public class FormPrinter {

    PrintRequestAttributeSet attributes;
    DocFlavor psInFormat;
    PrintService printService;
    private static final Logger logger = LoggerFactory.getLogger(FormPrinter.class);

    public FormPrinter(){
        this.attributes = new HashPrintRequestAttributeSet();
        this.attributes.add(new Copies(1));
        this.attributes.add(MediaSizeName.ISO_A4);
        this.attributes.add(Sides.ONE_SIDED);

        this.psInFormat = DocFlavor.INPUT_STREAM.AUTOSENSE;
        this.printService = PrintServiceLookup.lookupDefaultPrintService();
    }

    public PrintService[] getPrintServices(){
        return PrinterJob.lookupPrintServices();
    }

    public List<PrinterDevice> getPrinterDevices(){
        return Arrays.stream(getPrintServices()).map(PrinterDevice::new).collect(Collectors.toList());
    }

    public void printWithPrinter(PrintService printService, ApplicationForm formDocument){

        DocPrintJob printJob = printService.createPrintJob();

        try (InputStream psStream = new ByteArrayInputStream(new PdfDocument(formDocument).getOutputStream().toByteArray())) {
            printJob.print(new SimpleDoc(psStream, psInFormat, null), attributes);

        } catch (IOException | PrintException e) {
            logger.error(e.getMessage());
        }
    }

    public String getPrinterName(PrintService printService) {
        try {
            return printService.getAttribute(PrinterName.class).getValue();
        } catch (Exception e) {
            return printService.getName();
        }
    }

    class PrinterDevice{
        private PrintService printService;
        private String name;
        public PrinterDevice(PrintService printService){
            this.printService = printService;
            this.name = getPrinterName(printService);
        }

        public String getName() {
            return this.name;
        }

        public PrintService getPrintService() {
            return this.printService;
        }
    }
}
