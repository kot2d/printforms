package ru.printforms.services;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.spring.annotation.UIScope;

@Route("print_preview")
@UIScope
@CssImport("./styles/print_main_layout_style.css")
public class PrintPreview extends VerticalLayout implements HasUrlParameter<String> {

    private String toPersonHeaderText;
    private Span toPersonPart;
    private Span fromPersonPart;
    private Span title;
    private Span body;
    private Span signerLastNameWithInitials;
    private Span signDate;

    @Override
    public void setParameter(BeforeEvent event, String parameter) {
        String[] parameters = parameter.split("&");
        this.toPersonPart.setText(parameters[0]);
        this.fromPersonPart.setText(parameters[1]);
        this.title.setText(parameters[2]);
        this.body.setText(parameters[3]);
        this.signerLastNameWithInitials.setText(parameters[4]);
        this.signDate.setText(parameters[5]);
        UI.getCurrent().getPage().executeJs("print();");
        UI.getCurrent().getPage().executeJs("window.history.back();");
    }

    public PrintPreview() {
        VerticalLayout mainLayout = new VerticalLayout();
        mainLayout.setClassName("print_preview_main_layout");
        mainLayout.setHeight("297mm");
        mainLayout.setWidth("210mm");

        VerticalLayout headerLayout =  new VerticalLayout();
        this.toPersonPart = new Span();
        toPersonPart.setWidthFull();
        this.fromPersonPart = new Span();
        fromPersonPart.setWidthFull();
        headerLayout.setHeight("90mm");
        headerLayout.setWidth("70mm");
        headerLayout.setSpacing(false);
        headerLayout.setMargin(false);
        headerLayout.setPadding(false);

        VerticalLayout titleLayout = new VerticalLayout();
        this.title = new Span("ЗАЯВЛЕНИЕ");
        titleLayout.setHorizontalComponentAlignment(Alignment.CENTER,title);
        titleLayout.add(title);
        titleLayout.setWidthFull();

        this.body = new Span();
        body.setWidthFull();

        VerticalLayout signLayout = new VerticalLayout();
        signLayout.setWidth("100mm");
        this.signerLastNameWithInitials = new Span("Иванов К.В.");
        this.signDate = new Span("10.10.2020");
        signLayout.setHorizontalComponentAlignment(Alignment.END, signerLastNameWithInitials);
        signLayout.setHorizontalComponentAlignment(Alignment.END, signDate);
        signLayout.add(signerLastNameWithInitials, signDate);

        mainLayout.setHorizontalComponentAlignment(Alignment.END, headerLayout);
        mainLayout.setHorizontalComponentAlignment(Alignment.END, signLayout);

        headerLayout.add(toPersonPart, fromPersonPart);
        mainLayout.add(headerLayout, titleLayout, body, signLayout);
        add(mainLayout);
    }
}

