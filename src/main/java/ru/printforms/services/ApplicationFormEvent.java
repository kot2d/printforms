package ru.printforms.services;

import java.util.ArrayList;
import java.util.List;

public class ApplicationFormEvent {

    private List<ApplicationFormListener> listeners = new ArrayList<>();

    public void addListener(ApplicationFormListener listener){
        listeners.add(listener);
    }

    public void updateHeader(){
        for(ApplicationFormListener listener : this.listeners){
            listener.updateHeader();
        }
    }

    public void updateBody(){
        for(ApplicationFormListener listener : this.listeners){
            listener.updateBody();
        }
    }

    public void updateFooter(){
        for(ApplicationFormListener listener : this.listeners){
            listener.updateFooter();
        }
    }
}
