package ru.printforms.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.stream.Collectors;

public class ExternalFileReader {

    private ApplicationContext springAppCtx;
    private static Logger logger = LoggerFactory.getLogger(ExternalFileReader.class);

    public ExternalFileReader(ApplicationContext springAppCtx){
        this.springAppCtx = springAppCtx;
    }

    public List<String> read(String filePath){
        try {
            InputStream resource = springAppCtx.getResource("file:" + filePath).getInputStream();
            try ( BufferedReader reader = new BufferedReader(
                    new InputStreamReader(resource, "UTF8")) ) {
                return reader.lines().collect(Collectors.toList());
                }
            }
        catch (IOException e){
            logger.error("File: " + filePath + "не найден!");
            return null;
        }
    }
}
