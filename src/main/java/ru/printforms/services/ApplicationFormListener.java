package ru.printforms.services;

public interface ApplicationFormListener {
    void updateHeader();
    void updateBody();
    void updateFooter();
}
