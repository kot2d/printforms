package ru.printforms.services.russiancases;

public class CasesAdapter {

    public static Case getDative(){
        return new DativeCase();
    }

    public static Case getGenitive(){
        return new GenitiveCase();
    }
}
