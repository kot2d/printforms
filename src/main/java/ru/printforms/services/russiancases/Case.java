package ru.printforms.services.russiancases;

import java.util.Arrays;
import java.util.List;

public abstract class Case {
    final static List<String> vowels = Arrays.asList("а", "у", "о", "ы", "и", "э", "я", "ю", "ё", "е");
    final static List<String> consonants = Arrays.asList("б", "в", "г", "д", "ж", "з", "к", "л", "м", "н", "п", "р", "с"
            , "т", "ф", "х", "ц", "ч", "ш", "щ");

    public abstract String getNounInCase(String noun);

    public abstract String getAdjectiveInCase(String adjective);

    public abstract String getLastNameInCase(String lastName);

    public abstract String getFirstNameInCase(String firstName);

    public abstract String getMiddleNameInCase(String middleName);
}
