package ru.printforms.services.russiancases;

import java.util.Arrays;
import java.util.List;

public class DativeCase extends Case{

    @Override
    public String getNounInCase(String noun){
        if(noun.endsWith("а")){
            return noun.substring(0, noun.length()-1) + "е";
        } else if(noun.endsWith("я")){
            if(noun.endsWith("ия")){
                return  noun.substring(0, noun.length()-1) + "и";
            } else return  noun.substring(0, noun.length()-1) + "я";
        } else if(noun.endsWith("ь") || noun.endsWith("й")){
            return noun.substring(0, noun.length()-1) + "ю";
        } else if(noun.endsWith("о")){
            return noun.substring(0, noun.length()-1) + "y";
        } else if(consonants.contains(String.valueOf(noun.charAt(noun.length()-1)))) {
            return noun + "y";
        }
        return noun;
    }

    @Override
    public String getAdjectiveInCase(String adjective){
        if (adjective.endsWith("ый") || adjective.endsWith("ое") || adjective.endsWith("ой")) {
            return adjective.substring(0, adjective.length() - 2) + "ому";
        } else if (adjective.endsWith("ий")){
            if (adjective.endsWith("ший") || adjective.endsWith("щий")){
                return adjective.substring(0, adjective.length() - 2) + "уму";
            } else return adjective.substring(0, adjective.length() - 2) + "ому";
        } else if (adjective.endsWith("ая")){
            return adjective.substring(0, adjective.length() - 2) + "ой";
        }
        return adjective;
    }

    @Override
    public String getLastNameInCase(String lastName) {

        List<String> lastNameGroup1 = Arrays.asList("е", "и", "о", "у", "ы", "э", "ю");
        List<String> lastNameGroup2 = Arrays.asList("ая", "уя", "оя", "ыя", "ия", "яя", "эя", "юя", "ёя", "ея");

        if (lastName.endsWith("ов") || lastName.endsWith("eв")) {
            return lastName + "у";
        } else if (lastName.endsWith("ва") || lastName.endsWith("на")) {
            return lastName.substring(0, lastName.length() - 1) + "ой";
        } else if (lastName.endsWith("ин") || lastName.endsWith("ын")) {
            return lastName + "у";
        } else if (lastName.endsWith("ский") || lastName.endsWith("цкий")) {
            return lastName.substring(0, lastName.length() - 2) + "ому";
        } else if (lastName.endsWith("ых") || lastName.endsWith("их")) {
            return lastName;
        } else if (lastNameGroup1.contains(String.valueOf(lastName.charAt(lastName.length() - 1)))) {
            return lastName;
        } else if (lastNameGroup2.contains(lastName.substring(lastName.length() - 2))) {
            return lastName.substring(0, lastName.length() - 2) + "ой";
        } else if (lastName.endsWith("ь")) {
            return lastName;
        }
        return lastName;
    }

    @Override
    public String getFirstNameInCase(String firstName) {
        if (firstName.endsWith("й")) {
            return firstName.substring(0, firstName.length() - 1) + "ю";
        } else if (consonants.contains(String.valueOf(firstName.charAt(firstName.length() - 1)))) {
            if(firstName.endsWith("ел")){
                return firstName.substring(0, firstName.length() - 2) + "лу";
            } else return firstName + "у";
        } else if (firstName.endsWith("я")) {
            return firstName.substring(0, firstName.length() - 1) + "и";
        } else if (firstName.endsWith("а")) {
            return firstName.substring(0, firstName.length() - 1) + "е";
        }
        return firstName;
    }

    @Override
    public String getMiddleNameInCase(String middleName) {
        if (middleName == null) return "";
        if (middleName.endsWith("ич")) {
            return middleName + "у";
        } else if (middleName.endsWith("вна")) {
            return middleName.substring(0, middleName.length() - 1) + "е";
        }
        return middleName;
    }
}
