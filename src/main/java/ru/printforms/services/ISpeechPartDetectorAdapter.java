package ru.printforms.services;

public interface ISpeechPartDetectorAdapter {

    boolean isAdjectives(String part);
    boolean isNoun(String part);

}
