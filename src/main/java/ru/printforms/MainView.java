package ru.printforms;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.HtmlImport;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.theme.Theme;
import com.vaadin.flow.theme.material.Material;

@Route("")
@Theme(Material.class)
@HtmlImport("./styles/material_theme_style.html")
public class MainView extends VerticalLayout{

    public MainView() {
        VerticalLayout dataContainer = new VerticalLayout();
        VerticalLayout workingTimeFormLayout = new VerticalLayout();
        VerticalLayout vacationFormLayout = new VerticalLayout();
        VerticalLayout paymentFormLayout = new VerticalLayout();
        H3 workingTimeFormLabel = new H3("Рабочий график");
        workingTimeFormLayout.add(workingTimeFormLabel
                , new LabelButton("Заявление для изменения рабочего графика", ChangeWorkingTimeFormView.class).getButton()
                , new LabelButton("Заявление для прихода/ухода", ComingLateTimeFormView.class).getButton());
        H3 vacationFormLabel = new H3("Отпуск");
        vacationFormLayout.add(vacationFormLabel
                , new LabelButton("Заявление оплачиваемого отпуска", PaidVacationFormView.class).getButton()
                , new LabelButton("Заявление неоплачиваемого отпуска", UnpaidVacationFormView.class).getButton()
                , new LabelButton("Заявление отпуска за счет выходного", VacationDayForWeekendWorkFormView.class).getButton());
        H3 paymentFormLabel = new H3("Выплаты");
        paymentFormLayout.add(paymentFormLabel
                , new LabelButton("Заявление на выплату за работу в выходной", PaymentForWeekendFormView.class).getButton()
                );
        dataContainer.add(workingTimeFormLayout, vacationFormLayout, paymentFormLayout);
        setHorizontalComponentAlignment(Alignment.CENTER, dataContainer);
        add(dataContainer);
    }

    class LabelButton{
        Button button;
        public LabelButton(String name, Class formView){
            button = new Button(name, new Icon(VaadinIcon.ARROW_RIGHT));
            button.addClickListener(event -> {UI.getCurrent().navigate(formView);});
        }
        public Button getButton() {
            return button;
        }
    }
}