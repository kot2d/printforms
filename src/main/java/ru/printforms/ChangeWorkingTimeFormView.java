package ru.printforms;

import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.timepicker.TimePicker;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;

import org.springframework.beans.factory.annotation.Autowired;
import ru.printforms.domain.SecuredUser;
import ru.printforms.domain.applicationForms.ChangeWorkingTimeApplicationForm;
import ru.printforms.services.ApplicationFormEvent;
import ru.printforms.services.ApplicationFormLayout;
import ru.printforms.services.utills.FormDateTimeFormatter;
import ru.printforms.services.vaadin.DateTimePeriodLayout;
import ru.printforms.services.vaadin.FormProperties;
import ru.printforms.services.vaadin.binders.ChangeWorkingTimeFormBinder;

import java.util.Objects;

@SpringComponent
@Route("change_working_time_form")
@UIScope
public class ChangeWorkingTimeFormView extends VerticalLayout {

    private ChangeWorkingTimeApplicationForm document;

    private DateTimePeriodLayout timePeriodLayout = new DateTimePeriodLayout.Builder(FormProperties.INPUT_WIDTH_INT)
            .withTimePeriod()
            .withDescription("Укажите время ухода и прихода:")
            .build();

    private DateTimePeriodLayout datePeriodLayout = new DateTimePeriodLayout.Builder(FormProperties.INPUT_WIDTH_INT)
            .withDatePeriod()
            .withCheckPeriodOrOneTime("На один день")
            .withDescription("Укажите даты начала и окончания нового графика:")
            .build();

    private DatePicker startDatePicker = datePeriodLayout.getFirstDay();
    private DatePicker endDatePicker = datePeriodLayout.getEndDate();
    private Checkbox ondDayGraphicCheckbox = datePeriodLayout.getPeriodOrOneTime();
    private TimePicker startTimePicker = timePeriodLayout.getStartTime();
    private TimePicker endTimePicker = timePeriodLayout.getEndTime();
    private ApplicationFormLayout<ChangeWorkingTimeApplicationForm> appFormLayout;
    ApplicationFormEvent applicationFormEvent = new ApplicationFormEvent();

    public ChangeWorkingTimeFormView(@Autowired SecuredUser user) {
        this.document = new ChangeWorkingTimeApplicationForm();
        this.setPadding(false);
        ChangeWorkingTimeFormBinder formBinder =
                new ChangeWorkingTimeFormBinder(document, new Binder<>(ChangeWorkingTimeApplicationForm.class));
        this.appFormLayout = new ApplicationFormLayout<>(user, formBinder, this.document, this.applicationFormEvent);
        VerticalLayout inputLayout = this.appFormLayout.getCustomInputLayout();

        this.buildBody();

        formBinder.bindStartTime(this.startTimePicker);
        formBinder.bindEndTime(this.endTimePicker, this.startTimePicker);
        formBinder.bindStartDate(this.startDatePicker);
        formBinder.bindEndDate(this.endDatePicker, this.startDatePicker);
        //formBinder.bindOneDayGraphic(this.ondDayGraphicCheckbox);

        startTimePicker.addValueChangeListener(valueChangeEvent -> {
            this.buildBody();
        });
        endTimePicker.addValueChangeListener(valueChangeEvent -> {
            this.buildBody();
        });
        this.startDatePicker.addValueChangeListener(valueChangeEvent -> {
            this.endDatePicker.setValue(Objects.requireNonNull(this.startDatePicker.getValue()).plusMonths(1));
            this.buildBody();
        });

        ondDayGraphicCheckbox.addValueChangeListener(event -> {
            if(ondDayGraphicCheckbox.getValue()){
                this.endDatePicker.setVisible(false);
                this.buildBody();
            }else {
                this.endDatePicker.setVisible(true);
                this.buildBody();
            }
        });

        endDatePicker.addValueChangeListener(valueChangeEvent -> {
            this.buildBody();
        });

        inputLayout.add(this.timePeriodLayout, this.datePeriodLayout);
        this.add(appFormLayout);
    }

    private void buildBody(){
        if(!this.ondDayGraphicCheckbox.getValue()) {
            this.document.setBody((this.startTimePicker.getValue()==null) ? FormDateTimeFormatter.getEmptyTime() : FormDateTimeFormatter.getTime(this.startTimePicker.getValue())
                    , (this.endTimePicker.getValue()==null) ? FormDateTimeFormatter.getEmptyTime() : FormDateTimeFormatter.getTime(this.endTimePicker.getValue())
                    , (this.startDatePicker.getValue()==null) ? FormDateTimeFormatter.getEmptyDate() : FormDateTimeFormatter.getDate(this.startDatePicker.getValue())
                    , (this.endDatePicker.getValue()==null) ? FormDateTimeFormatter.getEmptyDate() : FormDateTimeFormatter.getDate(this.endDatePicker.getValue())
                    , "семейными обстоятельствами");
        } else {
            this.document.setBody((this.startTimePicker.getValue()==null) ? FormDateTimeFormatter.getEmptyTime() : FormDateTimeFormatter.getTime(this.startTimePicker.getValue())
                    , (this.endTimePicker.getValue()==null) ? FormDateTimeFormatter.getEmptyTime() : FormDateTimeFormatter.getTime(this.endTimePicker.getValue())
                    , (this.startDatePicker.getValue()==null) ? FormDateTimeFormatter.getEmptyDate() : FormDateTimeFormatter.getDate(this.startDatePicker.getValue())
                    , "семейными обстоятельствами");
        }
        this.applicationFormEvent.updateBody();
    }
    
}
