package ru.printforms;

import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.spring.annotation.UIScope;
import org.springframework.beans.factory.annotation.Autowired;
import ru.printforms.domain.SecuredUser;
import ru.printforms.domain.applicationForms.PaymentForWeekendApplicationForm;
import ru.printforms.services.ApplicationFormEvent;
import ru.printforms.services.ApplicationFormLayout;
import ru.printforms.services.vaadin.binders.PaymentForWeekendFormBinder;
import ru.printforms.services.vaadin.layouts.DatePickerDynamicCollectionLayout;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Route("payment_for_weekend_form")
@UIScope
public class PaymentForWeekendFormView extends VerticalLayout {

    private PaymentForWeekendApplicationForm document;
    private ApplicationFormLayout<PaymentForWeekendApplicationForm> appFormLayout;
    ApplicationFormEvent applicationFormEvent = new ApplicationFormEvent();

    public PaymentForWeekendFormView(@Autowired SecuredUser user) {
        this.document = new PaymentForWeekendApplicationForm();
        this.setPadding(false);

        PaymentForWeekendFormBinder formBinder =
                new PaymentForWeekendFormBinder(document, new Binder<>(PaymentForWeekendApplicationForm.class));

        this.appFormLayout = new ApplicationFormLayout<>(user, formBinder, this.document, this.applicationFormEvent);

        VerticalLayout inputLayout = this.appFormLayout.getCustomInputLayout();



        DatePickerDynamicCollectionLayout datePickerCollectionLayout = new DatePickerDynamicCollectionLayout();
        formBinder.bindWeekendDate(datePickerCollectionLayout.getStaticDatePicker());
        datePickerCollectionLayout.getStaticDatePicker().addValueChangeListener(changeEvent ->{
            this.buildBody(datePickerCollectionLayout.getValues());
        });

        datePickerCollectionLayout.getAddDateButton().addClickListener(clickEvent ->{
            datePickerCollectionLayout.getNewDatePicker().addValueChangeListener(changeEvent ->{
                this.buildBody(datePickerCollectionLayout.getValues());
            });
            datePickerCollectionLayout.getNewRemoveButton().addClickListener(removeEvent ->{
                this.buildBody(datePickerCollectionLayout.getValues());
            });
        });
        this.document.setBody(datePickerCollectionLayout.getStaticDatePicker().getValue());
        this.applicationFormEvent.updateBody();
        inputLayout.add(datePickerCollectionLayout);
        this.add(appFormLayout);
    }

    void buildBody(List<LocalDate> dateList){
        if(dateList.size()>1){
            this.document.setBody(dateList.stream().sorted().collect(Collectors.toList()));
        } else this.document.setBody(dateList.get(0));
        this.applicationFormEvent.updateBody();
    }
}
