package ru.printforms;

import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.spring.annotation.UIScope;
import org.springframework.beans.factory.annotation.Autowired;
import ru.printforms.domain.SecuredUser;
import ru.printforms.domain.applicationForms.VacationForWeekendApplicationForm;
import ru.printforms.services.ApplicationFormEvent;
import ru.printforms.services.ApplicationFormLayout;
import ru.printforms.services.vaadin.FormDatePicker;
import ru.printforms.services.vaadin.binders.VacationDayForWeekendWorkBinder;

import static java.time.DayOfWeek.SATURDAY;
import static java.time.temporal.TemporalAdjusters.next;

@Route("weekend_work_for_vacation_day")
@UIScope
public class VacationDayForWeekendWorkFormView  extends VerticalLayout {

    private VacationForWeekendApplicationForm document;
    private final FormDatePicker vacationDatePicker = new FormDatePicker("День отгула");
    private final FormDatePicker weekendDatePicker = new FormDatePicker("Раб.выходной");

    private ApplicationFormLayout<VacationForWeekendApplicationForm> appFormLayout;
    ApplicationFormEvent applicationFormEvent = new ApplicationFormEvent();

    public VacationDayForWeekendWorkFormView(@Autowired SecuredUser user) {
        this.document = new VacationForWeekendApplicationForm();

        VacationDayForWeekendWorkBinder formBinder =
                new VacationDayForWeekendWorkBinder(document, new Binder<>(VacationForWeekendApplicationForm.class));

        formBinder.bindWeekendDate(weekendDatePicker);
        formBinder.bindVacationDate(vacationDatePicker);

        this.appFormLayout = new ApplicationFormLayout<>(user, formBinder, this.document, this.applicationFormEvent);

        VerticalLayout inputLayout = this.appFormLayout.getCustomInputLayout();

        vacationDatePicker.addValueChangeListener(valueChangeEvent -> {
            weekendDatePicker.setValue(vacationDatePicker.getValue().with(next(SATURDAY)));
            this.buildBody();
        });
        weekendDatePicker.addValueChangeListener(valueChangeEvent -> {
            this.buildBody();
        });
        inputLayout.add(this.vacationDatePicker, this.weekendDatePicker);
        this.add(appFormLayout);
        this.buildBody();
    }

    private void buildBody(){
        this.document.setBody(this.vacationDatePicker.getValue()
                , this.weekendDatePicker.getValue()
                , "семейными обстоятельствами");
        this.applicationFormEvent.updateBody();
    }
}

