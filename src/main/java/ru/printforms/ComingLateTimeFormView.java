package ru.printforms;

import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.timepicker.TimePicker;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.spring.annotation.UIScope;
import org.springframework.beans.factory.annotation.Autowired;
import ru.printforms.domain.SecuredUser;
import ru.printforms.domain.applicationForms.ComingLateApplicationForm;
import ru.printforms.services.ApplicationFormEvent;
import ru.printforms.services.ApplicationFormLayout;
import ru.printforms.services.vaadin.DateTimePeriodLayout;
import ru.printforms.services.vaadin.FormDatePicker;
import ru.printforms.services.vaadin.FormProperties;
import ru.printforms.services.vaadin.binders.ComingLateFormBinder;

@Route("set_late_time_form")
@UIScope
public class ComingLateTimeFormView extends VerticalLayout {

    private DateTimePeriodLayout timePeriodLayout = new DateTimePeriodLayout.Builder(FormProperties.INPUT_WIDTH_INT)
            .withTimePeriod()
            .withDescription("Укажите время ухода и прихода:")
            .build();

    private final FormDatePicker lateDatePicker = new FormDatePicker("День");
    private final TimePicker startTimePicker = timePeriodLayout.getStartTime();
    private final TimePicker endTimePicker = timePeriodLayout.getEndTime();

    private ApplicationFormLayout<ComingLateApplicationForm> appFormLayout;
    ApplicationFormEvent applicationFormEvent = new ApplicationFormEvent();

    ComingLateApplicationForm document;

    public ComingLateTimeFormView(@Autowired SecuredUser user) {
        this.document = new ComingLateApplicationForm();

        ComingLateFormBinder formBinder =
                new ComingLateFormBinder(document, new Binder<>(ComingLateApplicationForm.class));

        this.appFormLayout = new ApplicationFormLayout<>(user, formBinder, this.document, this.applicationFormEvent);

        VerticalLayout inputLayout = this.appFormLayout.getCustomInputLayout();

        formBinder.bindDate(this.lateDatePicker);
        formBinder.bindStartTime(this.startTimePicker);
        formBinder.bindEndTime(this.endTimePicker, this.startTimePicker);

        startTimePicker.addValueChangeListener(valueChangeEvent -> {
            this.buildBody();
        });
        endTimePicker.addValueChangeListener(valueChangeEvent -> {
            this.buildBody();
        });
        lateDatePicker.addValueChangeListener(valueChangeEvent -> {
            this.buildBody();
        });
        inputLayout.add(this.timePeriodLayout, this.lateDatePicker);
        this.add(appFormLayout);
    }

    private void buildBody(){
        this.document.setBody(this.startTimePicker.getValue()
                , this.endTimePicker.getValue()
                , lateDatePicker.getValue()
                , "семейными обстоятельствами");
        this.applicationFormEvent.updateBody();
    }
}
