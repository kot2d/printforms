package ru.printforms;

import com.vaadin.flow.server.BootstrapListener;
import com.vaadin.flow.server.BootstrapPageResponse;
import com.vaadin.flow.server.ServiceInitEvent;
import com.vaadin.flow.server.VaadinServiceInitListener;
import com.vaadin.flow.spring.annotation.SpringComponent;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

/**
 * Appends {@code meta} tag to the bootstrap page.
 */
@SpringComponent
public class AddMetaServiceInitListener
        implements VaadinServiceInitListener, BootstrapListener {

    @Override
    public void serviceInit(ServiceInitEvent event) {
        event.addBootstrapListener(this);
    }

    @Override
    public void modifyBootstrapPage(BootstrapPageResponse response) {
        Document document = response.getDocument();
        Element head = document.head();
        Element meta = head.appendElement("meta");
        meta.attr("name", "keywords");
        meta.attr("content", "Flow, Spring, Polymer");
    }

}