package ru.printforms.domain;

import ru.printforms.services.ISpeechPartDetectorAdapter;
import ru.printforms.services.SpeechPartDetector;
import ru.printforms.services.russiancases.Case;
import ru.printforms.services.russiancases.CasesAdapter;

import java.util.Arrays;
import java.util.Objects;

public class PersonPosition {

    private ISpeechPartDetectorAdapter speechPartDetector = new SpeechPartDetector();

    private String[] departmentNameArray;
    private String[] positionNameArray;

    public PersonPosition(String departmentName, String positionName){
        String dptName = Objects.requireNonNull(departmentName);
        String psnName = Objects.requireNonNull(positionName);
        this.departmentNameArray = dptName.toLowerCase().split("\\s+");
        this.positionNameArray = psnName.toLowerCase().split("\\s+");
    }

    public String getDepartmentNameInGenitiveCase(){
        return this.formatWithCase(CasesAdapter.getGenitive()
                , Arrays.copyOf(this.departmentNameArray, this.departmentNameArray.length));
    }

    public String getDepartmentNameInDativeCase(){
        return this.formatWithCase(CasesAdapter.getDative()
                , Arrays.copyOf(this.departmentNameArray, this.departmentNameArray.length));
    }

    public String getPositionNameInGenitiveCase(){
        return this.formatWithCase(CasesAdapter.getGenitive()
                , Arrays.copyOf(this.positionNameArray, this.positionNameArray.length));
    }

    public String getPositionNameInDativeCase(){
        return this.formatWithCase(CasesAdapter.getDative()
                , Arrays.copyOf(this.positionNameArray, this.positionNameArray.length));
    }

    public String getDeclinedPositionAndDepartment(){
        return this.getPositionNameInGenitiveCase() + " " + this.getDepartmentNameInGenitiveCase();
    }

    private String formatWithCase(Case languageCase, String[] positionNameArray){
        //Если первое слово существительное, переводим его форму падежа
        if(speechPartDetector.isNoun(positionNameArray[0])){
            //если словов составное, то необходимо перевести в форму падежа обе его части
            if(positionNameArray[0].contains("-")){
                String[] parts = positionNameArray[0].split("-");
                positionNameArray[0] = languageCase.getNounInCase(parts[0]) + "-"
                        + languageCase.getNounInCase(parts[1]);
            } else positionNameArray[0] = languageCase.getNounInCase(positionNameArray[0]);
        }
        //если первое слово прилагательное
        if(positionNameArray.length > 1 & speechPartDetector.isAdjectives(positionNameArray[0])){
            //проход по всем словам, если они прилагательные переводим из в форму падежа, если попалось
            //существительное то переводим его в падеж и останавливаем цикл
            for (int i = 0; i<=positionNameArray.length; i++){
                if(speechPartDetector.isAdjectives(positionNameArray[i])){
                    positionNameArray[i] = languageCase.getAdjectiveInCase(positionNameArray[i]);
                } else if(speechPartDetector.isNoun(positionNameArray[i])){
                    //если словов составное, то необходимо перевести в форму падежа обе его части
                    if(positionNameArray[i].contains("-")){
                        String[] parts = positionNameArray[i].split("-");
                        positionNameArray[i] = languageCase.getNounInCase(parts[0]) + "-"
                                + languageCase.getNounInCase(parts[1]);
                    } else {
                        positionNameArray[i] = languageCase.getNounInCase(positionNameArray[i]);
                    }
                    break;
                }
            }
        }
        return String.join(" ", positionNameArray);
    }
}
