package ru.printforms.domain;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
//import org.springframework.security.core.Authentication;
//import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
//import ru.printforms.services.CustomUserDetails;
import ru.printforms.services.LocalDataCollections;

import javax.annotation.PostConstruct;

@Component
@Scope("session")
public class SecuredUser {

    private Person person;
    private Company company;

    @Autowired
    LocalDataCollections localDataCollections;

    public SecuredUser(){
    }

    @PostConstruct
    public void init(){
        //authentication = SecurityContextHolder.getContext().getAuthentication();
        //CustomUserDetails userDetails = (CustomUserDetails) authentication.getPrincipal();
        this.company = new OsposCompany();
        String userName = "ivanov";
        Names names = new Names("Иванов Константин Вячеславович");
        PersonPosition position;
        PersonPosition localPosition = localDataCollections.getUserPosition(userName);
        if(localPosition != null) position = localPosition;
        else position = new PersonPosition("отдел ОСПОС", "ведущий технический аналитик-руководитель проектов");
        this.person = new Person(userName, names, position);
    }

    public Company getCompany(){
        return this.company;
    }

    public Person getPerson(){
        return this.person;
    }

}
