package ru.printforms.domain;

public class OsposCompany extends Company{
    public OsposCompany(){
        super("АО"
                , "ОСПОС"
                , new Person("", new Names("Мочалов", "Евгений", "Владимирович")
                             , new PersonPosition("Администрация", "Генеральный директор"))
                , new Person("", new Names("Барышникова", "Лариса", "Сергеевна")
                        , new PersonPosition("Администрация", "Заместитель генерального директора"))
             );
    }
}
