package ru.printforms.domain;

public class FormConstants {
    public final static String HEADER_BAR = "Замeстителю генерального директора АО \u00ABОСПОС\u00BB Барышниковой Л.С.";
    public final static String HEADER_MOCH = "Генеральному директору  АО \u00ABОСПОС\u00BB Мочалову Е.В.";
    public final static String DOCTITLE = "Заявление";

}
