package ru.printforms.domain;


import java.util.Objects;

public class Person {

    private Names names;
    private PersonPosition position;
    private String userName;

    public Person(String userName, Names names, PersonPosition position){
        this.userName = Objects.requireNonNull(userName);
        this.names = Objects.requireNonNull(names);
        this.position = Objects.requireNonNull(position);
    }

    public Names getNames() {
        return names;
    }

    public PersonPosition getPosition() {
        return position;
    }

    public String getUserName(){ return userName; }
}
