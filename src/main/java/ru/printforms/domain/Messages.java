package ru.printforms.domain;

public class Messages{
    public final static String EMPTY_FIELD = "Необходимо заполнить поле";
    public final static String START_DATE_EARLIER_END_DATE = "День начала графика не может быть позже дня завершения графика";
    public final static String START_TIME_EARLIER_END_TIME = "Время прихода может не быть позже времени завершения рабочего дня";
}
