package ru.printforms.domain;

import ru.printforms.services.russiancases.CasesAdapter;

public class Names {

    private String firstName;
    private String lastName;
    private String middleName;

    public Names(String lastName, String firstName, String middleName){
        this.lastName = lastName.trim();
        this.firstName= firstName.trim();;
        this.middleName = middleName.trim();;
    }

    public Names(String fullName){
        String[] fullNameArray = fullName.split("\\s+");
        if (fullNameArray.length==3){
            this.lastName = fullNameArray[0];
            this.firstName = fullNameArray[1];
            this.middleName = fullNameArray[2];
        }
        if (fullNameArray.length==2){
            this.lastName = fullNameArray[0];
            this.firstName = fullNameArray[1];
        }
    }

    public String getShortFullName(){
        if (this.middleName != null){
            return this.lastName + " " + this.firstName.charAt(0) + ". " + this.middleName.charAt(0) +".";
        } else {
            return this.lastName + " " + this.firstName.charAt(0) + ".";
        }
    }

    public String getFullNameInitialsInGenitiveCase(){
        if (this.middleName != null){
            return CasesAdapter.getGenitive().getLastNameInCase(this.lastName) + " " + this.firstName.charAt(0) + ". " +
                    this.middleName.charAt(0) +".";
        } else {
            return CasesAdapter.getGenitive().getLastNameInCase(this.lastName) + " " + this.firstName.charAt(0) + ".";
        }
    }

    public String getFullNameInitialsInDativeCase(){
        if (this.middleName != null){
            return this.getLastNameInDativeCase() + " " + this.firstName.charAt(0) + ". " +
                    this.middleName.charAt(0) +".";
        } else {
            return this.getLastNameInDativeCase() + " " + this.firstName.charAt(0) + ".";
        }
    }

    public String getFirstNameInGenitiveCase(){
        return CasesAdapter.getGenitive().getFirstNameInCase(this.firstName);
    }

    public String getLastNameInGenitiveCase(){
        return CasesAdapter.getGenitive().getLastNameInCase(this.lastName);
    }

    public String getMiddleNameInGenitiveCase(){
        return CasesAdapter.getGenitive().getMiddleNameInCase(this.middleName);
    }

    public String getFirstNameInDativeCase(){
        return CasesAdapter.getDative().getFirstNameInCase(this.firstName);
    }

    public String getLastNameInDativeCase(){
        return CasesAdapter.getDative().getLastNameInCase(this.lastName);
    }

    public String getMiddleNameInDativeCase(){
        return CasesAdapter.getDative().getMiddleNameInCase(this.middleName);
    }

    public String getNames(){
        return this.lastName + " " + this.firstName + " " + this.middleName;
    }
}
