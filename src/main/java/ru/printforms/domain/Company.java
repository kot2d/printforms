package ru.printforms.domain;

import ru.printforms.services.utills.TextFormatter;

public class Company {

    private String organisationType, name;
    private Person globalChief, localChief;

    public Company(String organisationType, String name, Person globalChief, Person localChief){
        this.name = name;
        this.organisationType = organisationType;
        this.globalChief = globalChief;
        this.localChief = localChief;
    }

    public String getOrganisationType() {
        return organisationType;
    }

    public Person getGlobalChief() {
        return globalChief;
    }

    public Person getLocalChief() {
        return localChief;
    }

    public String getName() {
        return name;
    }

    public String getFullName(){
        return TextFormatter.replaceSpaceWithNonBreakingSpace(this.organisationType + " \u00AB" + this.name + "\u00BB");
    }

}
