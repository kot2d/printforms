package ru.printforms.domain.applicationForms;

import ru.printforms.services.utills.TextFormatter;

public class Header{
    private String toPersonPosition, companyName, toPersonFullName, fromPersonPosition, fromPersonDepartment, fromPersonFullName;


    public Header(String toPersonPosition, String companyName, String toPersonFullName, String fromPersonPosition
            , String fromPersonDepartment, String fromPersonFullName){
        this.toPersonPosition = toPersonPosition;
        this.companyName = companyName;
        this.toPersonFullName = toPersonFullName;
        this.fromPersonPosition = fromPersonPosition;
        this.fromPersonDepartment = fromPersonDepartment;
        this.fromPersonFullName = fromPersonFullName;
    }

    public String getToPersonPart(){
        return buildToPersonPart(this.toPersonPosition, this.companyName, this.toPersonFullName);
    }

    public String getFromPersonPart(){
        return buildFromPersonPart(this.fromPersonPosition, this.fromPersonDepartment, this.fromPersonFullName);
    }

    public String buildToPersonPart(String toPersonPosition, String companyName, String toPersonFullName){
        return TextFormatter.addNonBreakingSpaceToFio(String.format("%s %s %s", toPersonPosition
                , companyName, toPersonFullName));
    }
    public String buildFromPersonPart(String fromPersonPosition, String fromPersonDepartment, String fromPersonFullName){
        return TextFormatter.addNonBreakingSpaceToFio(String.format("от %s %s %s",fromPersonPosition, fromPersonDepartment, fromPersonFullName));
    }
}