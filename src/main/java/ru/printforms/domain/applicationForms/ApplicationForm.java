package ru.printforms.domain.applicationForms;

import java.time.LocalDate;

public class ApplicationForm {

    private String companyName;
    private String toPersonPosition;
    private String toPersonLastNameWithInitials;
    final private String title = "ЗАЯВЛЕНИЕ";
    protected String body;
    private LocalDate documentDate;
    private String signerLastNameWithInitials;
    private String fromPersonPosition;
    private String fromPersonDepartment;
    private String fromPersonLastNameWithInitials;

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getToPersonPosition() {
        return toPersonPosition;
    }

    public void setToPersonPosition(String toPersonPosition) {
        this.toPersonPosition = toPersonPosition;
    }

    public String getToPersonLastNameWithInitials() {
        return toPersonLastNameWithInitials;
    }

    public void setToPersonLastNameWithInitials(String toPersonFullName) {
        this.toPersonLastNameWithInitials = toPersonFullName;
    }

    public String getTitle() {
        return title;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public LocalDate getDocumentDate() {
        return documentDate;
    }

    public void setDocumentDate(LocalDate documentDate) {
        this.documentDate = documentDate;
    }

    public String getSignerLastNameWithInitials() {
        return signerLastNameWithInitials;
    }

    public void setSignerLastNameWithInitials(String documentSigner) {
        this.signerLastNameWithInitials = documentSigner;
    }

    public String getFromPersonPosition() {
        return fromPersonPosition;
    }

    public void setFromPersonPosition(String fromPersonPosition) {
        this.fromPersonPosition = fromPersonPosition;
    }

    public String getFromPersonDepartment() {
        return fromPersonDepartment;
    }

    public void setFromPersonDepartment(String fromPersonDepartment) {
        this.fromPersonDepartment = fromPersonDepartment;
    }

    public String getFromPersonLastNameWithInitials() {
        return fromPersonLastNameWithInitials;
    }

    public void setFromPersonLastNameWithInitials(String fromPersonFullName) {
        this.fromPersonLastNameWithInitials = fromPersonFullName;
    }

    public Header getHeader(){
        return new Header(this.toPersonPosition, this.companyName, this.toPersonLastNameWithInitials
                , this.fromPersonPosition, this.fromPersonDepartment, this.fromPersonLastNameWithInitials);
    }


    public String getBody(){
        return this.body;
    };
}

