package ru.printforms.domain.applicationForms;

import ru.printforms.services.utills.FormDateTimeFormatter;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

public class PaymentForWeekendApplicationForm extends ApplicationForm {

    private LocalDate weekendDate;

    public LocalDate getWeekendDate() {
        return weekendDate;
    }

    public void setWeekendDate(LocalDate weekendDate) {
        this.weekendDate = weekendDate;
    }

    public void setBody(LocalDate weekendDate){
        String weekendDay = (weekendDate==null) ? FormDateTimeFormatter.getEmptyDate() : FormDateTimeFormatter.getDate(weekendDate);
        this.body = String.format("Прошу оплатить работу в выходной день %s в двойном размере."
                , weekendDay);
    }

    public void setBody(List<LocalDate> weekendDates){
        String list = weekendDates.stream()
                .map(date -> FormDateTimeFormatter.getDate(date))
                .collect(Collectors.joining(", "));
        this.body = String.format("Прошу оплатить работу в выходные дни %s в двойном размере."
                , list);
    }
}
