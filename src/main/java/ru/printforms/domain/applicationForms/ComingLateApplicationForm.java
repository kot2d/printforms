package ru.printforms.domain.applicationForms;

import ru.printforms.services.utills.FormDateTimeFormatter;

import java.time.LocalDate;
import java.time.LocalTime;

public class ComingLateApplicationForm extends ApplicationForm {

    private LocalTime startTime = null;
    private LocalTime endTime = null;
    private LocalDate date = null;

    public void setDocumentDate(LocalDate documentDate)
    {
        super.setDocumentDate(documentDate);
    }

    public LocalTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalTime startTime) {
        this.startTime = startTime;
    }

    public LocalTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalTime endTime) {
        this.endTime = endTime;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public void setBody(LocalTime startTime, LocalTime endTime, LocalDate date, String reason){
        this.body = String.format("Прошу разрешить мне отсутствовать на рабочем месте %s c %s по %s в связи с %s."
                , (date==null) ? FormDateTimeFormatter.getEmptyDate() : FormDateTimeFormatter.getDate(date)
                , (startTime==null) ? FormDateTimeFormatter.getEmptyTime() : FormDateTimeFormatter.getTime(startTime)
                , (endTime==null) ? FormDateTimeFormatter.getEmptyTime() : FormDateTimeFormatter.getTime(endTime)
                , reason);
    }
}
