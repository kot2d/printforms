package ru.printforms.domain.applicationForms;

import java.time.LocalDate;
import java.time.LocalTime;

public class ChangeWorkingTimeApplicationForm extends ApplicationForm {

    private LocalTime startTime;
    private LocalTime endTime;
    private LocalDate startDate;
    private LocalDate endDate;
    private boolean oneDayGraphic = false;

    public LocalTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalTime startTime) {
        this.startTime = startTime;
    }

    public LocalTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalTime endTime) {
        this.endTime = endTime;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public boolean getOneDayGraphic(){
        return this.oneDayGraphic;
    }

    public void setOneDayGraphic(boolean oneDayGraphic) {
        this.oneDayGraphic = oneDayGraphic;
    }

    public void setBody(String startTime, String endTime, String startDate, String endDate, String reason){
        this.body = String.format("Прошу разрешить мне исполнять мои служебные обязанности по графику с %s по " +
                "%s в период с %s по %s в связи с %s.", startTime, endTime, startDate, endDate, reason);
    }

    public void setBody(String startTime, String endTime, String startDate, String reason){
        this.body = String.format("Прошу разрешить мне исполнять мои служебные обязанности по графику с %s по " +
                "%s %s в связи с %s.", startTime, endTime, startDate, reason);
    }
}