package ru.printforms.domain.applicationForms;

public class ChangeWorkingTimeFormDocumentBody {
    private String body;
    public ChangeWorkingTimeFormDocumentBody(String startTime, String endTime, String startDate, String endDate, String reason){
        this.body = String.format("Прошу разрешить мне исполнять мои служебные обязанности по графику с %s по " +
                        "%s в период с %s по %s в связи с %s.", startTime, endTime, startDate, endDate, reason);
    }

    public ChangeWorkingTimeFormDocumentBody(String startTime, String endTime, String startDate, String reason){
        this.body = String.format("Прошу разрешить мне исполнять мои служебные обязанности по графику с %s по " +
                "%s %s в связи с %s.", startTime, endTime, startDate, reason);
    }

    public String getBody(){
        return this.body;
    }
}
