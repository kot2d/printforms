package ru.printforms.domain.applicationForms;

import ru.printforms.services.utills.FormDateTimeFormatter;
import ru.printforms.services.utills.TextFormatter;

import java.time.LocalDate;

import static java.time.temporal.ChronoUnit.DAYS;

public class UnpaidVacationApplicationForm extends ApplicationForm{
    private LocalDate startDate = null;
    private LocalDate endDate = null;

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public void setDocumentDate(LocalDate documentDate)
    {
        super.setDocumentDate(documentDate);
    }

    public void setBody(LocalDate startDate, LocalDate endDate){
        long daysBetween = 0;
        String literalDays = "_";
        if(startDate!=null && endDate!=null) {
            daysBetween = DAYS.between(startDate, endDate) + 1;
            literalDays = (daysBetween==0) ? "_" : TextFormatter.getAmountLiterally(daysBetween);
        }
        String startDay = (startDate==null) ? FormDateTimeFormatter.getEmptyDate() : FormDateTimeFormatter.getDate(startDate);
        String endDay = (endDate==null) ? FormDateTimeFormatter.getEmptyDate() : FormDateTimeFormatter.getDate(endDate);
        this.body = String.format("Прошу предоставить мне отпуск без сохранения заработной платы на %s(%s)" +
                " календарных дней с %s по %s.", daysBetween, literalDays, startDay, endDay);
    }

    public void setBody(LocalDate startDate){
        String startDay = (startDate==null) ? FormDateTimeFormatter.getEmptyDate() : FormDateTimeFormatter.getDate(startDate);
        this.body = String.format("Прошу предоставить мне отпуск без сохранения заработной платы на 1(один)" +
                " календарный день %s.", startDay);
    }
}
