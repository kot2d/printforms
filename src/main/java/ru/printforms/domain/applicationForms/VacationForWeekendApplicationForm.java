package ru.printforms.domain.applicationForms;

import ru.printforms.services.utills.FormDateTimeFormatter;

import java.time.LocalDate;

public class VacationForWeekendApplicationForm extends ApplicationForm {

    private LocalDate vacationDay;
    private LocalDate weekendDay;

    public void setBody(LocalDate vacationDay, LocalDate weekendDay, String reason){
        this.body = String.format("Прошу предоставить мне дополнительный день отдыха %s в счет работы " +
                        "в выходной день %s в связи с %s."
                , (vacationDay==null) ? FormDateTimeFormatter.getEmptyDate() : FormDateTimeFormatter.getDate(vacationDay)
                , (weekendDay==null) ? FormDateTimeFormatter.getEmptyDate() : FormDateTimeFormatter.getDate(weekendDay)
                , reason);
    }

    public LocalDate getVacationDay() {
        return vacationDay;
    }

    public void setVacationDay(LocalDate vacationDay){this.vacationDay = vacationDay; }

    public LocalDate getWeekendDay() {
        return weekendDay;
    }

    public void setWeekendDay(LocalDate weekendDay){this.weekendDay = weekendDay;}
}
