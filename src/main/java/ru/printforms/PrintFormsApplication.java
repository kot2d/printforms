package ru.printforms;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.servlet.error.ErrorMvcAutoConfiguration;


@SpringBootApplication(exclude = ErrorMvcAutoConfiguration.class)
public class PrintFormsApplication {

    private static final Logger logger = LoggerFactory.getLogger(PrintFormsApplication.class);

    public static void main(String[] args) {

        SpringApplication.run(PrintFormsApplication.class, args);
        logger.info("PrintFormApplication starts");
    }



}
